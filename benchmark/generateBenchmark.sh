#!/bin/sh
# Benchmark script
#
# This script generates the benchmark for time series motif set discovery algorithms.

rm -rf ../benchmark/timeSeriesMotifPairBenchmark
mkdir ../benchmark/timeSeriesMotifPairBenchmark
rm -rf ../benchmark/timeSeriesMotifSetBenchmark
mkdir ../benchmark/timeSeriesMotifSetBenchmark

echo "Generating time series with induced time series motif pairs."
echo "************************************************************"

for itrType in "box" "triangle" "semicircle" "trapezoid" "positiveflank" "negativeflank" "sine" "cosine"; do
    for itrHeight in "200.0" "300.0"; do
        for itrRandom in "0.1" "0.2"; do
            echo "Generating time series with 10000 values, motif set type $itrType, subsequence length 100, subsequence height $itrHeight and randomness factor $itrRandom."
            ./TSGenerator -rd $itrRandom -l 10000 -w 100 -mp $itrType $itrHeight
        done
    done
done

echo "Generating time series with 10000 values, motif set type box, subsequence length 200, subsequence height 300.0 and randomness factor 0.2."
./TSGenerator -rd 0.2 -l 10000 -w 200 -mp box 300.0
echo "Generating time series with 10000 values, motif set type trapezoid, subsequence length 200, subsequence height 300.0 and randomness factor 0.2."
./TSGenerator -rd 0.2 -l 10000 -w 200 -mp trapezoid 300.0
echo "Generating time series with 10000 values, motif set type semicircle, subsequence length 100, subsequence height 300.0 and randomness factor 0.2."
./TSGenerator -rd 0.2 -l 10000 -w 100 -mp semicircle 300.0
echo "Generating time series with 10000 values, motif set type cosine, subsequence length 100, subsequence height 300.0 and randomness factor 0.2."
./TSGenerator -rd 0.2 -l 10000 -w 100 -mp cosine 300.0
echo "Generating time series with 10000 values, motif set type sine, subsequence length 100, subsequence height 400.0 and randomness factor 0.1."
./TSGenerator -rd 0.1 -l 10000 -w 100 -mp sine 400.0
echo "Generating time series with 10000 values, motif set type box, subsequence length 200, subsequence height 300.0 and randomness factor 0.01."
./TSGenerator -rd 0.01 -l 10000 -w 100 -mp box 300.0
echo "Generating time series with 10000 values, motif set type box, subsequence length 100, subsequence height 300.0 and randomness factor 0.3."
./TSGenerator -rd 0.3 -l 10000 -w 100 -mp box 300.0

mv time_series_* ../benchmark/timeSeriesMotifPairBenchmark

echo "Generating time series with induced time series motif sets."
echo "***********************************************************"

for itrType in "box" "triangle" "semicircle" "trapezoid" "positiveflank" "negativeflank" "sine" "cosine"; do
    for itrHeight in "200.0" "300.0"; do
        for itrRandom in "0.1" "0.2"; do
            echo "Generating time series with 10000 values, motif set type $itrType, subsequence length 100, subsequence height $itrHeight and randomness factor $itrRandom."
            ./TSGenerator -rd $itrRandom -l 10000 -w 100 -msl $itrType 3 $itrHeight
        done
    done
done

echo "Generating time series with 10000 values, motif set type box, sine, semicircle, subsequence length 200, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.1."
./TSGenerator -rd 0.1 -l 10000 -w 200 -msl box 3 300.0 sine 5 300.0 semicircle 8 300.0
echo "Generating time series with 10000 values, motif set type box, trapezoid, positiveflank, subsequence length 200, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.1."
./TSGenerator -rd 0.1 -l 10000 -w 200 -msl box 2 300.0 trapezoid 3 300.0 positiveflank 5 300.0
echo "Generating time series with 10000 values, motif set type box, trapezoid, semicircle, subsequence length 200, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.1."
./TSGenerator -rd 0.1 -l 10000 -w 200 -msl box 3 300.0 trapezoid 6 300.0 semicircle 7 300.0
echo "Generating time series with 10000 values, motif set type triangle, semicircle, semicircle, subsequence length 200, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.1."
./TSGenerator -rd 0.1 -l 10000 -w 200 -msl triangle 4 300.0 semicircle 5 300.0 semicircle 6 300.0
echo "Generating time series with 10000 values, motif set type box, triangle, semicircle, subsequence length 200, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.1."
./TSGenerator -rd 0.1 -l 10000 -w 200 -msl box 3 300.0 triangle 5 300.0 semicircle 7 300.0
echo "Generating time series with 10000 values, motif set type box, box, box, subsequence length 200, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.1."
./TSGenerator -rd 0.1 -l 10000 -w 200 -msl box 5 300.0 box 6 300.0 box 7 300.0
echo "Generating time series with 10000 values, motif set type positiveflank, sine, semicircle, subsequence length 200, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.1."
./TSGenerator -rd 0.1 -l 10000 -w 200 -msl positiveflank 5 300.0 sine 6 300.0 semicircle 7 300.0
echo "Generating time series with 10000 values, motif set type negativeflank, sine, semicircle, subsequence length 100, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.2."
./TSGenerator -rd 0.2 -l 10000 -w 100 -msl negativeflank 3 300.0 sine 3 300.0 semicircle 6 300.0
echo "Generating time series with 10000 values, motif set type positiveflank, cosine, triangle, subsequence length 100, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.2."
./TSGenerator -rd 0.2 -l 10000 -w 100 -msl positiveflank 3 300.0 cosine 5 300.0 triangle 6 300.0
echo "Generating time series with 10000 values, motif set type semicircle, sine, cosine, subsequence length 100, subsequence height 300.0, 400.0, 200.0 and randomness factor 0.1."
./TSGenerator -rd 0.1 -l 10000 -w 100 -msl semicircle 3 300.0 sine 3 400.0 cosine 6 200.0
echo "Generating time series with 10000 values, motif set type box, sine, semicircle, subsequence length 100, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.01."
./TSGenerator -rd 0.01 -l 10000 -w 100 -msl box 3 300.0 sine 5 300.0 semicircle 7 300.0
echo "Generating time series with 10000 values, motif set type box, sine, semicircle, subsequence length 100, subsequence height 300.0, 300.0, 300.0 and randomness factor 0.3."
./TSGenerator -rd 0.3 -l 10000 -w 100 -msl box 3 300.0 sine 5 300.0 semicircle 6 300.0

mv time_series_* ../benchmark/timeSeriesMotifSetBenchmark
