#!/bin/sh
# Benchmark script
#
# This script runs the benchmark on various time series motif set discovery algorithms.

#if example
#if [ "$X" -lt "$Y" ]; then echo "true"; else echo "false"; fi

#get n-th line of file
#sed "nq;d" filename | cut -f 1 -d ','

#function example
#myfunc() {
#    echo "I was called as: $@"
#}

#while loop
#while [true]
#do
#echo "."
#done

#in: filepath out: window size
getWindowSize() {
    windowSize=$(sed '2q;d' $1 | cut -f 1 -d ',')
}

#in: motif positions file out: array of position points
getPositions() {
    count=3
    arrPos=()
    position=$(sed "$((count))q;d" $1 | cut -f 1 -d ',')
    
    while [ "$position" ]
    do
        count=$((count+1));
        countWin=0;
        while [[ $countWin -lt $windowSize ]];
        do
            arrPos[$((position+countWin))]="true";
            countWin=$((countWin+1));
        done;
        position=$(sed "$((count))q;d" $1 | cut -f 1 -d ',');
    done
}

#in: algorithm results file path out: get first motif array
getFirstMotifSet() {
    count=1
    arrM=()
    position=$(sed "$((count))q;d" $1)
    
    while [ ! "$position" = "next" ] && [ ! "$position" = "end" ];
    do
        count=$((count+1));
        countWin=0;
        while [[ $countWin -lt $windowSize ]];
        do
            re='^[0-9]+$';
            if [[ $((position+countWin)) =~ $re ]];
            then
                arrM[$((position+countWin))]="true";
            fi;
            
            countWin=$((countWin+1));
        done;
        position=$(sed "$((count))q;d" $1);
    done
    
    count=$((count+1));
}

#in: result file path out: next motif array
getNextMotifSet() {
    position=$(sed "$((count))q;d" $1)
    
    while [ ! "$position" = "next" ] && [ ! "$position" = "end" ];
    do
        count=$((count+1));
        countWin=0;
        while [[ $countWin -lt $windowSize ]];
        do
            arrM[$((position+countWin))]="true";
            countWin=$((countWin+1));
        done;
        position=$(sed "$((count))q;d" $1);
    done
    
    count=$((count+1));
}

#out: true positives and false positives
calculatePrecision()  {
    itrTS=0
    tp=0
    fp=0
    
    while [[ $itrTS -lt 10000 ]];
    do
        if [ "${arrM[$itrTS]}" = "true" ] && [ "${arrPos[$itrTS]}" = "true" ];
        then
            tp=$((tp+1));
        fi;
        itrTS=$((itrTS+1));
    done
    
    fp=$((${#arrM[@]}-tp))
}

#out: true positives and false negatives
calculateRecall() {
    fn=${#arrPos[@]}
    
    fn=$((fn-tp))
}

#in: found motifs file path, output file path
printStats() {
    getFirstMotifSet $1 $3
    
    calculatePrecision
    calculateRecall
    
    bestTP=$tp
    bestFP=$fp
    bestFN=$fn
    
    while [ "$position" = "next" ];
    do
        getNextMotifSet $1;
        
        calculatePrecision;
        calculateRecall;
        
        #check which motif set is best
        denOldPre=$((bestTP+bestFP));
        denOldRec=$((bestTP+bestFN));
        denNewPre=$((tp+fp));
        denNewRec=$((tp+tn));
        old=$((bestTP*denOldPre*denNewPre*denNewRec+bestTP*denOldRec*denNewPre*denNewRec));
        new=$((tp*denNewPre*denOldPre*denOldRec+tp*denNewRec*denOldPre*denOldRec));
        
        if [ "$new" -gt "$old" ];
        then
            bestTP=$tp;
            bestFP=$fp;
            bestFN=$fn;
        fi;
    done
    
    #print statistic to file
    echo "$bestTP, $bestFP, $bestFN" >> $2
}

#in: file path positions, file path algorithm results, output file path
generateStats() {
    getWindowSize $1
    getPositions $1
    printStats $2 $3 $1
}

########################################
# Time Series Top Motif Pair Discovery #
########################################

rm -f mkStats.csv
rm -f mpStats.csv
rm -f lmStats.csv
rm -f emmaStats.csv
rm -f gvStats.csv

for itrLoop in {0..38..1}; do
    generateStats timeSeriesMotifPairBenchmark/time_series_$itrLoop/time_series_motif_set_positions_0.csv timeSeriesMotifPairBenchmark/time_series_$itrLoop/mk.csv mkStats.csv;
    generateStats timeSeriesMotifPairBenchmark/time_series_$itrLoop/time_series_motif_set_positions_0.csv timeSeriesMotifPairBenchmark/time_series_$itrLoop/mp.csv mpStats.csv;
done

#######################################
# Time Series Top Motif Set Discovery #
#######################################

for itrLoop in {0..43..1}; do
    generateStats timeSeriesMotifSetBenchmark/time_series_$itrLoop/time_series_motif_set_positions_0.csv timeSeriesMotifSetBenchmark/time_series_$itrLoop/lm.csv lmStats.csv;
    generateStats timeSeriesMotifSetBenchmark/time_series_$itrLoop/time_series_motif_set_positions_0.csv timeSeriesMotifSetBenchmark/time_series_$itrLoop/emma.csv emmaStats.csv;
    generateStats timeSeriesMotifSetBenchmark/time_series_$itrLoop/time_series_motif_set_positions_0.csv timeSeriesMotifSetBenchmark/time_series_$itrLoop/gv.csv gvStats.csv;
done
