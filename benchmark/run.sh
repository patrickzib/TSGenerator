#!/bin/sh

sh generateBenchmark.sh

sh runBenchmark.sh

sh generateStats.sh

Rscript generatePlots.R
