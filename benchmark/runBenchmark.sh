#!/bin/sh
# Benchmark script
#
# This script runs the benchmark on various time series motif set discovery algorithms.

########################################
# Time Series Top Motif Pair Discovery #
########################################
#www.cs.ucr.edu/%7Emueen/zip/MK_code.zip
#MK algorithm: mk_l file timeserieslength subsequencelength windowsize numberofreferencepoints
#./mk timeSeriesMotifPairBenchmark/time_series_0.csv 10000 40 40 10

for itr in {0..38..1}; do
    ./mk timeSeriesMotifPairBenchmark/time_series_$itr/time_series_0.csv 10000 100 100 10 > timeSeriesMotifPairBenchmark/time_series_$itr/mk.csv
done

./mk timeSeriesMotifPairBenchmark/time_series_32/time_series_0.csv 10000 200 200 10 > timeSeriesMotifPairBenchmark/time_series_32/mk.csv
./mk timeSeriesMotifPairBenchmark/time_series_33/time_series_0.csv 10000 200 200 10 > timeSeriesMotifPairBenchmark/time_series_33/mk.csv
./mk timeSeriesMotifPairBenchmark/time_series_34/time_series_0.csv 10000 100 100 10 > timeSeriesMotifPairBenchmark/time_series_34/mk.csv
./mk timeSeriesMotifPairBenchmark/time_series_35/time_series_0.csv 10000 100 100 10 > timeSeriesMotifPairBenchmark/time_series_35/mk.csv
./mk timeSeriesMotifPairBenchmark/time_series_36/time_series_0.csv 10000 100 100 10 > timeSeriesMotifPairBenchmark/time_series_36/mk.csv
./mk timeSeriesMotifPairBenchmark/time_series_37/time_series_0.csv 10000 100 100 10 > timeSeriesMotifPairBenchmark/time_series_37/mk.csv
./mk timeSeriesMotifPairBenchmark/time_series_38/time_series_0.csv 10000 100 100 10 > timeSeriesMotifPairBenchmark/time_series_38/mk.csv

##https://github.com/javidlakha/matrix-profile
#Matrix Profile I: mp -W ignore file windowsize
#python -W ignore mp.py timeSeriesMotifPairBenchmark/time_Series_0.csv 40

for itr in {0..38..1}; do
    python -W ignore mp.py timeSeriesMotifPairBenchmark/time_Series_$itr/time_series_0.csv 100 > timeSeriesMotifPairBenchmark/time_series_$itr/mp.csv
done

python -W ignore mp.py timeSeriesMotifPairBenchmark/time_Series_32/time_series_0.csv 200 > timeSeriesMotifPairBenchmark/time_series_32/mp.csv
python -W ignore mp.py timeSeriesMotifPairBenchmark/time_Series_33/time_series_0.csv 200 > timeSeriesMotifPairBenchmark/time_series_33/mp.csv
python -W ignore mp.py timeSeriesMotifPairBenchmark/time_Series_34/time_series_0.csv 100 > timeSeriesMotifPairBenchmark/time_series_34/mp.csv
python -W ignore mp.py timeSeriesMotifPairBenchmark/time_Series_35/time_series_0.csv 100 > timeSeriesMotifPairBenchmark/time_series_35/mp.csv
python -W ignore mp.py timeSeriesMotifPairBenchmark/time_Series_36/time_series_0.csv 100 > timeSeriesMotifPairBenchmark/time_series_36/mp.csv
python -W ignore mp.py timeSeriesMotifPairBenchmark/time_Series_37/time_series_0.csv 100 > timeSeriesMotifPairBenchmark/time_series_37/mp.csv
python -W ignore mp.py timeSeriesMotifPairBenchmark/time_Series_38/time_series_0.csv 100 > timeSeriesMotifPairBenchmark/time_series_38/mp.csv

#######################################
# Time Series Top Motif Set Discovery #
#######################################
#http://fs.ismll.de/publicspace/LearnMotifs/
#Learn Motifs: java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=200 K=3 pct=0.01
#java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=200 K=3 pct=0.01

for itr in {0..42..1}; do
    java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_$itr/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=100 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_$itr/lm.csv
done

java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_32/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=200 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_32/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_33/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=200 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_33/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_34/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=200 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_34/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_35/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=200 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_35/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_36/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=200 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_36/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_37/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=200 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_37/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_38/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=200 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_38/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_39/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=100 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_39/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_40/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=100 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_40/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_41/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=100 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_41/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_42/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=100 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_42/lm.csv
java -Xmx7g -jar LearnMotifs.jar dataSet=timeSeriesMotifSetBenchmark/time_Series_43/time_series_0.csv eta=0.3 maxIter=300 numRandomRestarts=1 alpha=2 tsLength=10000 w=100 K=3 pct=0.01 > timeSeriesMotifSetBenchmark/time_series_43/lm.csv

#https://github.com/jMotif/SAX
#EMMA: java -Xmx7g -jar EMMA.jar filename motifsize motifrange paasize alphabetsize snormalizationthreshold
#java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_0.csv 200 1.88 4 3 0.01

for itr in {0..31..1}; do
    java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_$itr/time_series_0.csv 100 $(head -1 timeSeriesMotifSetBenchmark/time_series_$itr/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_$itr/emma.csv
done

java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_32/time_series_0.csv 200 $(head -1 timeSeriesMotifSetBenchmark/time_series_32/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_32/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_33/time_series_0.csv 200 $(head -1 timeSeriesMotifSetBenchmark/time_series_33/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_33/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_34/time_series_0.csv 200 $(head -1 timeSeriesMotifSetBenchmark/time_series_34/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_34/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_35/time_series_0.csv 200 $(head -1 timeSeriesMotifSetBenchmark/time_series_35/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_35/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_36/time_series_0.csv 200 $(head -1 timeSeriesMotifSetBenchmark/time_series_36/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_36/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_37/time_series_0.csv 200 $(head -1 timeSeriesMotifSetBenchmark/time_series_37/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_37/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_38/time_series_0.csv 200 $(head -1 timeSeriesMotifSetBenchmark/time_series_38/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_38/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_39/time_series_0.csv 100 $(head -1 timeSeriesMotifSetBenchmark/time_series_39/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_39/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_40/time_series_0.csv 100 $(head -1 timeSeriesMotifSetBenchmark/time_series_40/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_40/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_41/time_series_0.csv 100 $(head -1 timeSeriesMotifSetBenchmark/time_series_41/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_41/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_42/time_series_0.csv 100 $(head -1 timeSeriesMotifSetBenchmark/time_series_42/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_42/emma.csv
java -Xmx7g -jar EMMA.jar timeSeriesMotifSetBenchmark/time_Series_43/time_series_0.csv 100 $(head -1 timeSeriesMotifSetBenchmark/time_series_43/time_series_0.csv | cut -f 1 -d ',') 4 3 0.01 > timeSeriesMotifSetBenchmark/time_series_43/emma.csv

#https://github.com/GrammarViz2/grammarviz2_src
#GrammarViz3: java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_0.csv --window_size 200 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01
#java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_0.csv --window_size 200 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01

for itr in {0..31..1}; do
    java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_$itr/time_series_0.csv --window_size 100 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_$itr/gv.csv
done

java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_32/time_series_0.csv --window_size 200 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_32/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_33/time_series_0.csv --window_size 200 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_33/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_34/time_series_0.csv --window_size 200 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_34/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_35/time_series_0.csv --window_size 200 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_35/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_36/time_series_0.csv --window_size 200 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_36/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_37/time_series_0.csv --window_size 200 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_37/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_38/time_series_0.csv --window_size 200 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_38/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_39/time_series_0.csv --window_size 100 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_39/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_40/time_series_0.csv --window_size 100 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_40/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_41/time_series_0.csv --window_size 100 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_41/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_42/time_series_0.csv --window_size 100 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_42/gv.csv
java -Xmx7g -jar GrammarViz3.jar --data_in timeSeriesMotifSetBenchmark/time_Series_43/time_series_0.csv --window_size 100 --alphabet_size 4 --word_size 6 --strategy EXACT --threshold 0.01 > timeSeriesMotifSetBenchmark/time_series_43/gv.csv
