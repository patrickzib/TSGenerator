set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-660.373483:627.536997]
set grid
set obj rect from 3381, graph 0 to 3480, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3261, graph 0 to 3360, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines