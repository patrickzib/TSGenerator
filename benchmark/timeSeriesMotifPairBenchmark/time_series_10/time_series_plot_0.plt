set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-622.929084:612.929221]
set grid
set obj rect from 6148, graph 0 to 6247, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6837, graph 0 to 6936, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines