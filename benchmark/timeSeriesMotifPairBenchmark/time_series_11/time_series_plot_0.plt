set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-810.542482:1677.125229]
set grid
set obj rect from 5339, graph 0 to 5438, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5637, graph 0 to 5736, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines