set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-631.303948:812.742035]
set grid
set obj rect from 3440, graph 0 to 3539, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3310, graph 0 to 3409, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines