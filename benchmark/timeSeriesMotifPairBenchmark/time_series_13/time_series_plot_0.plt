set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-732.223241:960.136673]
set grid
set obj rect from 9022, graph 0 to 9121, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3122, graph 0 to 3221, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines