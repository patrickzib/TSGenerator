set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-910.016343:788.715451]
set grid
set obj rect from 6936, graph 0 to 7035, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 308, graph 0 to 407, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines