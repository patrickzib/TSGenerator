set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-750.693316:838.784916]
set grid
set obj rect from 7241, graph 0 to 7340, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7103, graph 0 to 7202, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines