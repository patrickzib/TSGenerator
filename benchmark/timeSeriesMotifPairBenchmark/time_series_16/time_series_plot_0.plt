set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-888.815408:939.365061]
set grid
set obj rect from 6873, graph 0 to 6972, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4687, graph 0 to 4786, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines