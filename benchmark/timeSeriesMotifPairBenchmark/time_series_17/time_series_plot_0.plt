set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1436.356580:790.159106]
set grid
set obj rect from 8468, graph 0 to 8567, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 488, graph 0 to 587, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines