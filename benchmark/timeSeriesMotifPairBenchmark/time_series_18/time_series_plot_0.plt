set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-797.743504:636.167822]
set grid
set obj rect from 5111, graph 0 to 5210, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6666, graph 0 to 6765, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines