set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-703.808006:1323.667689]
set grid
set obj rect from 426, graph 0 to 525, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4679, graph 0 to 4778, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines