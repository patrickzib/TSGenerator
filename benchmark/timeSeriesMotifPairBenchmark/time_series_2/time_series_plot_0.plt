set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-820.579981:671.995487]
set grid
set obj rect from 6019, graph 0 to 6118, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8613, graph 0 to 8712, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines