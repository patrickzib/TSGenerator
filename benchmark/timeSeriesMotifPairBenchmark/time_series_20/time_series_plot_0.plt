set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1228.440084:540.107300]
set grid
set obj rect from 3606, graph 0 to 3705, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5377, graph 0 to 5476, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines