set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-752.082493:1045.698681]
set grid
set obj rect from 2227, graph 0 to 2326, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3291, graph 0 to 3390, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines