set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-964.528638:672.951326]
set grid
set obj rect from 4188, graph 0 to 4287, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3911, graph 0 to 4010, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines