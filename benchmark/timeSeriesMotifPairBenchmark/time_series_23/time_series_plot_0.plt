set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1076.089998:720.587104]
set grid
set obj rect from 7199, graph 0 to 7298, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5922, graph 0 to 6021, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines