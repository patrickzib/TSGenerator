set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-691.910659:1049.658883]
set grid
set obj rect from 4518, graph 0 to 4617, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5561, graph 0 to 5660, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines