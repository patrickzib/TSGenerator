set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-709.204500:642.832367]
set grid
set obj rect from 6044, graph 0 to 6143, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1161, graph 0 to 1260, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines