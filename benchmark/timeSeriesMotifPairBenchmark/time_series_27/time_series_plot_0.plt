set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-716.519028:992.844080]
set grid
set obj rect from 8558, graph 0 to 8657, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7326, graph 0 to 7425, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines