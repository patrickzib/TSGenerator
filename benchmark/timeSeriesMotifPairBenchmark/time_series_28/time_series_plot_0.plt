set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-771.418944:758.217825]
set grid
set obj rect from 8538, graph 0 to 8637, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3582, graph 0 to 3681, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines