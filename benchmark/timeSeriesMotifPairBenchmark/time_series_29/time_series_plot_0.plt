set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-892.649446:699.813171]
set grid
set obj rect from 5025, graph 0 to 5124, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7657, graph 0 to 7756, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines