set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-741.665423:952.657733]
set grid
set obj rect from 9213, graph 0 to 9312, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7205, graph 0 to 7304, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines