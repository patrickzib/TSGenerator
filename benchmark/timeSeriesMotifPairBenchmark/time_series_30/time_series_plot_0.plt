set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-654.081741:778.275038]
set grid
set obj rect from 4637, graph 0 to 4736, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1607, graph 0 to 1706, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines