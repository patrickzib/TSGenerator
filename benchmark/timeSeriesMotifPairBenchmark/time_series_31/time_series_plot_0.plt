set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1765.442077:848.005046]
set grid
set obj rect from 2983, graph 0 to 3082, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6563, graph 0 to 6662, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines