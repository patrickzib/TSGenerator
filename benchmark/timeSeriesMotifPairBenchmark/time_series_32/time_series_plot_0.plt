set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1044.287288:665.350576]
set grid
set obj rect from 213, graph 0 to 412, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1885, graph 0 to 2084, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines