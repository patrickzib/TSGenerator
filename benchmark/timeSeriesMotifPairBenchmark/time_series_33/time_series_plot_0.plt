set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-994.362941:806.066931]
set grid
set obj rect from 1753, graph 0 to 1952, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5029, graph 0 to 5228, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines