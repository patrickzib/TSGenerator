set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-835.069705:1773.042150]
set grid
set obj rect from 3844, graph 0 to 3943, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9198, graph 0 to 9297, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines