set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1146.699265:639.552952]
set grid
set obj rect from 1511, graph 0 to 1610, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8640, graph 0 to 8739, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines