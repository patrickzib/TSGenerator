set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-600.768421:1035.270858]
set grid
set obj rect from 2365, graph 0 to 2464, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4078, graph 0 to 4177, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines