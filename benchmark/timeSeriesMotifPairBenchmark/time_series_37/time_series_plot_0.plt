set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-125.708417:987.401415]
set grid
set obj rect from 8247, graph 0 to 8346, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2017, graph 0 to 2116, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines