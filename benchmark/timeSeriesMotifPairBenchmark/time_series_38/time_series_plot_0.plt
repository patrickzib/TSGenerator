set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-923.350277:874.607360]
set grid
set obj rect from 1215, graph 0 to 1314, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6655, graph 0 to 6754, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines