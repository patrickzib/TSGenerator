set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-615.174127:720.960231]
set grid
set obj rect from 1336, graph 0 to 1435, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6145, graph 0 to 6244, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines