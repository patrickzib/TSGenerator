set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-884.682044:644.598127]
set grid
set obj rect from 7735, graph 0 to 7834, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6467, graph 0 to 6566, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines