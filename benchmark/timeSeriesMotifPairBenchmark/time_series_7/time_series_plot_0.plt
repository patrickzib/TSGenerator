set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-702.498261:1019.727048]
set grid
set obj rect from 1771, graph 0 to 1870, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5166, graph 0 to 5265, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines