set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-846.493454:758.168261]
set grid
set obj rect from 4559, graph 0 to 4658, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1640, graph 0 to 1739, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines