set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-824.094827:896.810945]
set grid
set obj rect from 6834, graph 0 to 6933, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5427, graph 0 to 5526, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines