set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-612.060054:843.431866]
set grid
set obj rect from 7820, graph 0 to 7919, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8698, graph 0 to 8797, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8698, graph 0 to 8797, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7820, graph 0 to 7919, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2184, graph 0 to 2283, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines