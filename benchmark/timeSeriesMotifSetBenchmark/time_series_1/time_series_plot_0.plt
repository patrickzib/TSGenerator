set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-842.887108:1094.560195]
set grid
set obj rect from 5121, graph 0 to 5220, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2655, graph 0 to 2754, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2427, graph 0 to 2526, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2427, graph 0 to 2526, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2655, graph 0 to 2754, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines