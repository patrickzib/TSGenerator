set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-709.404694:1020.460230]
set grid
set obj rect from 8720, graph 0 to 8819, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1468, graph 0 to 1567, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 975, graph 0 to 1074, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 955, graph 0 to 1054, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1488, graph 0 to 1587, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines