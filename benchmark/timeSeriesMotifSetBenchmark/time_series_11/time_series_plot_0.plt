set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1091.361302:1213.289742]
set grid
set obj rect from 7517, graph 0 to 7616, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4790, graph 0 to 4889, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4536, graph 0 to 4635, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7512, graph 0 to 7611, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4795, graph 0 to 4894, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines