set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-658.470927:626.319874]
set grid
set obj rect from 7550, graph 0 to 7649, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7550, graph 0 to 7649, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1697, graph 0 to 1796, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1697, graph 0 to 1796, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6059, graph 0 to 6158, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines