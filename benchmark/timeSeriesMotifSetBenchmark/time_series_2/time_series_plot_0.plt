set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-701.881097:762.826873]
set grid
set obj rect from 6838, graph 0 to 6937, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6830, graph 0 to 6929, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2317, graph 0 to 2416, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5672, graph 0 to 5771, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5680, graph 0 to 5779, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines