set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1563.347543:792.934873]
set grid
set obj rect from 8197, graph 0 to 8296, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2651, graph 0 to 2750, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6905, graph 0 to 7004, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8197, graph 0 to 8296, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2651, graph 0 to 2750, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines