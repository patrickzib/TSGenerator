set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-766.157055:1808.373217]
set grid
set obj rect from 1986, graph 0 to 2085, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7304, graph 0 to 7403, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 773, graph 0 to 872, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1986, graph 0 to 2085, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7304, graph 0 to 7403, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines