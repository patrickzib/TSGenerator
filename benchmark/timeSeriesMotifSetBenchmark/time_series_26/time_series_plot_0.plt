set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1339.278771:721.005511]
set grid
set obj rect from 4879, graph 0 to 4978, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9338, graph 0 to 9437, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7444, graph 0 to 7543, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4879, graph 0 to 4978, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9338, graph 0 to 9437, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines