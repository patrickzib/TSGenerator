set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-729.651174:791.020469]
set grid
set obj rect from 8368, graph 0 to 8467, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5158, graph 0 to 5257, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5158, graph 0 to 5257, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8368, graph 0 to 8467, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1459, graph 0 to 1558, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines