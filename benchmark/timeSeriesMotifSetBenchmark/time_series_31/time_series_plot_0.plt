set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1127.540454:903.809719]
set grid
set obj rect from 2250, graph 0 to 2349, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1466, graph 0 to 1565, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6413, graph 0 to 6512, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6413, graph 0 to 6512, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1466, graph 0 to 1565, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines