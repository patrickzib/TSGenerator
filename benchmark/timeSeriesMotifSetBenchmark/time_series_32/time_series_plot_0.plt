set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-981.078362:1938.687395]
set grid
set obj rect from 6212, graph 0 to 6411, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5365, graph 0 to 5564, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3669, graph 0 to 3868, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3678, graph 0 to 3877, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6914, graph 0 to 7113, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 318, graph 0 to 517, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3090, graph 0 to 3289, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3099, graph 0 to 3298, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1143, graph 0 to 1342, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4849, graph 0 to 5048, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7860, graph 0 to 8059, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2600, graph 0 to 2799, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 917, graph 0 to 1116, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8401, graph 0 to 8600, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3313, graph 0 to 3512, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1433, graph 0 to 1632, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4385, graph 0 to 4584, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9523, graph 0 to 9722, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines