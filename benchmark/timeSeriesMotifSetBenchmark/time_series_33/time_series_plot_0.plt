set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-652.164946:1354.979893]
set grid
set obj rect from 117, graph 0 to 316, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3560, graph 0 to 3759, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4101, graph 0 to 4300, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4116, graph 0 to 4315, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4830, graph 0 to 5029, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9775, graph 0 to 9974, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8955, graph 0 to 9154, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8970, graph 0 to 9169, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5096, graph 0 to 5295, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8122, graph 0 to 8321, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6610, graph 0 to 6809, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3274, graph 0 to 3473, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines