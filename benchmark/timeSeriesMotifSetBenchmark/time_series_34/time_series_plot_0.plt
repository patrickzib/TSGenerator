set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1150.197178:1281.631122]
set grid
set obj rect from 9337, graph 0 to 9536, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1448, graph 0 to 1647, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3431, graph 0 to 3630, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7521, graph 0 to 7720, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8565, graph 0 to 8764, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6416, graph 0 to 6615, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7475, graph 0 to 7674, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3477, graph 0 to 3676, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8966, graph 0 to 9165, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2415, graph 0 to 2614, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2914, graph 0 to 3113, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6109, graph 0 to 6308, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 591, graph 0 to 790, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2087, graph 0 to 2286, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6845, graph 0 to 7044, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3946, graph 0 to 4145, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1827, graph 0 to 2026, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5445, graph 0 to 5644, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines