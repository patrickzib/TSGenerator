set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1221.559989:602.741013]
set grid
set obj rect from 2642, graph 0 to 2841, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7373, graph 0 to 7572, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9207, graph 0 to 9406, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6780, graph 0 to 6979, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1515, graph 0 to 1714, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6780, graph 0 to 6979, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3159, graph 0 to 3358, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7070, graph 0 to 7269, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5372, graph 0 to 5571, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7070, graph 0 to 7269, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3399, graph 0 to 3598, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1216, graph 0 to 1415, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7686, graph 0 to 7885, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 630, graph 0 to 829, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5661, graph 0 to 5860, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1924, graph 0 to 2123, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8391, graph 0 to 8590, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines