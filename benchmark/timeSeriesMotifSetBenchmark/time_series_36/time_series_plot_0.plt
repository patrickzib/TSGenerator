set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1181.361570:1707.259177]
set grid
set obj rect from 1118, graph 0 to 1317, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1478, graph 0 to 1677, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7572, graph 0 to 7771, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7572, graph 0 to 7771, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7365, graph 0 to 7564, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5133, graph 0 to 5332, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3218, graph 0 to 3417, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3218, graph 0 to 3417, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7881, graph 0 to 8080, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4079, graph 0 to 4278, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1716, graph 0 to 1915, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9351, graph 0 to 9550, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3775, graph 0 to 3974, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6833, graph 0 to 7032, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4669, graph 0 to 4868, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8390, graph 0 to 8589, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2156, graph 0 to 2355, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines