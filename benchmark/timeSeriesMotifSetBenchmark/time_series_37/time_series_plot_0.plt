set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-466.485227:1533.968486]
set grid
set obj rect from 828, graph 0 to 1027, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1165, graph 0 to 1364, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7337, graph 0 to 7536, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4039, graph 0 to 4238, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9258, graph 0 to 9457, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2544, graph 0 to 2743, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 599, graph 0 to 798, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9662, graph 0 to 9861, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 302, graph 0 to 501, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7076, graph 0 to 7275, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5650, graph 0 to 5849, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8617, graph 0 to 8816, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4030, graph 0 to 4229, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8232, graph 0 to 8431, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1836, graph 0 to 2035, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9653, graph 0 to 9852, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5277, graph 0 to 5476, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3106, graph 0 to 3305, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1402, graph 0 to 1601, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4738, graph 0 to 4937, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines