set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1009.565506:728.698239]
set grid
set obj rect from 1188, graph 0 to 1387, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5678, graph 0 to 5877, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4213, graph 0 to 4412, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7045, graph 0 to 7244, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6514, graph 0 to 6713, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 651, graph 0 to 850, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7606, graph 0 to 7805, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7944, graph 0 to 8143, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7944, graph 0 to 8143, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2014, graph 0 to 2213, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8279, graph 0 to 8478, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8601, graph 0 to 8800, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5150, graph 0 to 5349, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1488, graph 0 to 1687, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7045, graph 0 to 7244, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9295, graph 0 to 9494, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3382, graph 0 to 3581, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3971, graph 0 to 4170, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2397, graph 0 to 2596, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3587, graph 0 to 3786, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines