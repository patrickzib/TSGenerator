set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1693.261604:931.107887]
set grid
set obj rect from 4959, graph 0 to 5058, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9043, graph 0 to 9142, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7712, graph 0 to 7811, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6047, graph 0 to 6146, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7473, graph 0 to 7572, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6515, graph 0 to 6614, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 743, graph 0 to 842, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8688, graph 0 to 8787, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 397, graph 0 to 496, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4491, graph 0 to 4590, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5767, graph 0 to 5866, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8688, graph 0 to 8787, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4206, graph 0 to 4305, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6047, graph 0 to 6146, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines