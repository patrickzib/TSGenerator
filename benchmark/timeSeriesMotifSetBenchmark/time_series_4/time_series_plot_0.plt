set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-524.785883:889.685301]
set grid
set obj rect from 6984, graph 0 to 7083, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3232, graph 0 to 3331, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9642, graph 0 to 9741, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6984, graph 0 to 7083, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3232, graph 0 to 3331, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines