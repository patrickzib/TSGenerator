set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-3098.936687:681.143207]
set grid
set obj rect from 4216, graph 0 to 4315, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3370, graph 0 to 3469, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 538, graph 0 to 637, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8022, graph 0 to 8121, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6845, graph 0 to 6944, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 231, graph 0 to 330, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 119, graph 0 to 218, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4216, graph 0 to 4315, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5511, graph 0 to 5610, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4004, graph 0 to 4103, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8870, graph 0 to 8969, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2710, graph 0 to 2809, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 892, graph 0 to 991, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8022, graph 0 to 8121, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7246, graph 0 to 7345, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8271, graph 0 to 8370, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines