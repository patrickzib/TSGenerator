set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-801.278477:724.437076]
set grid
set obj rect from 442, graph 0 to 541, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 772, graph 0 to 871, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9021, graph 0 to 9120, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6086, graph 0 to 6185, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5257, graph 0 to 5356, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7749, graph 0 to 7848, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4242, graph 0 to 4341, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3542, graph 0 to 3641, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6086, graph 0 to 6185, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2513, graph 0 to 2612, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4482, graph 0 to 4581, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9398, graph 0 to 9497, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3542, graph 0 to 3641, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6779, graph 0 to 6878, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines