set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-234.608075:429.244149]
set grid
set obj rect from 5140, graph 0 to 5239, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5645, graph 0 to 5744, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2189, graph 0 to 2288, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4729, graph 0 to 4828, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2904, graph 0 to 3003, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6307, graph 0 to 6406, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7739, graph 0 to 7838, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4922, graph 0 to 5021, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7990, graph 0 to 8089, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2039, graph 0 to 2138, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5998, graph 0 to 6097, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4887, graph 0 to 4986, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2498, graph 0 to 2597, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4694, graph 0 to 4793, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4495, graph 0 to 4594, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7500, graph 0 to 7599, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4071, graph 0 to 4170, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines