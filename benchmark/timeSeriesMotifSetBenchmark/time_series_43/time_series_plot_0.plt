set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-1933.698821:1070.836757]
set grid
set obj rect from 2238, graph 0 to 2337, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2830, graph 0 to 2929, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9051, graph 0 to 9150, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 519, graph 0 to 618, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1229, graph 0 to 1328, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6570, graph 0 to 6669, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 816, graph 0 to 915, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4139, graph 0 to 4238, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 519, graph 0 to 618, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4321, graph 0 to 4420, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9497, graph 0 to 9596, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1766, graph 0 to 1865, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8298, graph 0 to 8397, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4139, graph 0 to 4238, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5720, graph 0 to 5819, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 8815, graph 0 to 8914, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines