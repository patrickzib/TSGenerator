set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-564.265658:1131.910805]
set grid
set obj rect from 5462, graph 0 to 5561, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1343, graph 0 to 1442, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 142, graph 0 to 241, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5462, graph 0 to 5561, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1343, graph 0 to 1442, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines