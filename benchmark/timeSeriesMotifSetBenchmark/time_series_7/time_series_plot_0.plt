set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-750.345288:1326.296210]
set grid
set obj rect from 7460, graph 0 to 7559, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 7460, graph 0 to 7559, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1008, graph 0 to 1107, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1008, graph 0 to 1107, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 5443, graph 0 to 5542, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines