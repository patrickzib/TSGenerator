set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-803.979353:1027.986927]
set grid
set obj rect from 1108, graph 0 to 1207, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 1108, graph 0 to 1207, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6659, graph 0 to 6758, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 6659, graph 0 to 6758, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 2996, graph 0 to 3095, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines