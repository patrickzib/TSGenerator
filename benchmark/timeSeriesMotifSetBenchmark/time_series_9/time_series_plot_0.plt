set terminal wxt size 1600, 300
set xrange[0:9999]
set yrange[-959.631362:907.993745]
set grid
set obj rect from 4526, graph 0 to 4625, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 4526, graph 0 to 4625, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9778, graph 0 to 9877, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 9778, graph 0 to 9877, graph 1 back fc lt -1 fs trans solid 0.1 noborder
set obj rect from 3193, graph 0 to 3292, graph 1 back fc lt -1 fs trans solid 0.1 noborder
plot "time_series_0.csv" notitle with lines