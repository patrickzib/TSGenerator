///\file basicmotifset.hpp
///
///\brief File contains the basic motif set class declaration.
///
///This is the header file of the basic motif set. The basic motif set consists of values in a list and an iterator on the next value
///that will be returned. Also, the basic motif set stroes the warp, scale, length and maximal height of the motif set.

#ifndef BASICMOTIF_HPP
#define BASICMOTIF_HPP

#include <vector>

using namespace std;

// ----------------------------------------------------------------------------
///\brief Class represents a basic motif set.
///
///The BasicMotifSet class contains the motif set values and an iterator to the next value location which will be next returned by the
///getNextValue(double *nextValue_out) function. Also, the basic motif set stroes the warp, scale, length and maximal height of the motif set.
///Besides the getFirstValue() and the getLastValue() member functions return the first and the last value of the motif set.
// ----------------------------------------------------------------------------
class BasicMotifSet {

protected:

    // ----------------------------------------------------------------------------
	///\brief This variable contains the warp of the motif set.
	///
	///This variable stores the warp of the motif set. The default warp is 1.0.
	// ----------------------------------------------------------------------------
	double warp = 1.0;

    // ----------------------------------------------------------------------------
	///\brief This variable contains the scale of the motif set.
	///
	///This variable stores the scale of the motif set. The default scale is 1.0.
	// ----------------------------------------------------------------------------
	double scale = 1.0;

    // ----------------------------------------------------------------------------
	///\brief This variable contains the length of the motif set.
	///
	///This variable stores the length of the motif set. The default length is 200.
	// ----------------------------------------------------------------------------
	size_t length = 200;

    // ----------------------------------------------------------------------------
	///\brief This variable contains the height of the motif set.
	///
	///This variable stores the height of the motif set. The default height of the motif set is 100.
	// ----------------------------------------------------------------------------
	double height = 100;

    // ----------------------------------------------------------------------------
	///\brief This variable contains the basic motif set subsequence values.
	///
	///This variable stores the basic motif set subsequence values. By default this values are empty.
	// ----------------------------------------------------------------------------
	vector<double> values;

    // ----------------------------------------------------------------------------
    ///\brief This motif set setter function sets the next value of the motif set.
    ///
    ///\param [in] value_in Hand over the next value added to motif subsequence.
    ///
    ///This function sets the next value of the motif set.
    // ----------------------------------------------------------------------------
	void addNextValue(double value_in);

public:

    // ----------------------------------------------------------------------------
    ///\brief This is the basic motif set constructor.
    ///
    ///\param [in] firstValue_in Hands over the first value of the motif set.
    ///
    ///The basic motif set constructor initializes the motif set.
    // ----------------------------------------------------------------------------
	BasicMotifSet();

    // ----------------------------------------------------------------------------
    ///\brief This motif set getter function returns the last value of the motif set.
    ///
    ///\return Returns the last value of the motif set.
    ///
    ///This function returns the last value of the motif set.
    // ----------------------------------------------------------------------------
	double getLastValue();
    
    // ----------------------------------------------------------------------------
    ///\brief This motif set getter function returns the subsequence.
    ///
    ///\param [out] subsequence_out Subsequence represented by this instance.
    ///
    ///This function returns the hole subsequence hold by the instance of this class.
    // ----------------------------------------------------------------------------
	void getSequence(vector<double> &subsequence_out);
};

#endif
