/** \file mk.hpp
 * 
 * \brief File contains the declaration of the mk algorithm written by Abdullah Al Mueen.
 * 
 * This is the header file of the corresponding mk algorithm written by Abdullah Al Mueen at the department of
 * Computer Science and Engineering of University of California - RIverside. The algorithm is based on the
 * paper EXACT DISCOVERY OF TIME SERIES MOTIFS by Abdullah Mueen, Eamonn Keogh and Qiang Zhu.
 */

#ifndef MK_HPP
#define MK_HPP

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <string>
#include <vector>
#include <algorithm>
#include <limits>
#include <iostream>

using namespace std;

// ----------------------------------------------------------------------------
///Calculates the distance between two time series x and y. If the distance is
///larger than the best so far (bsf) it stops computing and returns the approximate
///distance. To get exact distance the bsf argument should be omitted.
// ----------------------------------------------------------------------------
extern double distance(const vector<double> &x_in, const vector<double> &y_in, size_t length_in, double best_so_far = numeric_limits<double>::max());
extern double mk(const vector<double> &timeSeries_in, size_t windowSize_in, size_t &positionOne_out, size_t &positionTwo_out);
extern double mk_d(const vector<vector<double>> &timeSeriesDatabase_in, size_t windowSize_in);

#endif
