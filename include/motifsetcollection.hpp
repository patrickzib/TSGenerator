///\file motifsetcollection.hpp
/// 
/// \brief File contains the motif set collection declarations.
/// 
/// This is the header file of the motif set collection. The motif set collection consists of motif set class declarations.

#ifndef MOTIFCOLLECTION_HPP
#define MOTIFCOLLECTION_HPP

#define _USE_MATH_DEFINES

#include <basicmotifset.hpp>
#include <cmath>
#include <list>

using namespace std;

// ----------------------------------------------------------------------------
///\brief This class represents the box motif set.
/// 
/// The BoxMotifSet looks similiar to:
/// \code
/// ^
/// |
/// |      .........
/// | 
/// | 
/// | 
/// | .....         .....
/// O---------------------->
/// \endcode
// ----------------------------------------------------------------------------
class BoxMotifSet : public BasicMotifSet {

public:

    // ----------------------------------------------------------------------------
    ///\brief This is the box motif set definition.
    ///
    ///\param [in] &warp_in Hands over the warp of the .
    ///\param [in] &scale_in Hands over the scale of the box motif set.
    ///\param [in] &length_in Hands over the length of the box motif set.
    ///\param [in] &height_in Hands over the heigth of the box motif set.
    ///
    ///This is the box motif set definition. The motif set term warp x length has to be at least 3. Otherwise, the motif set is a two value line.
    // ----------------------------------------------------------------------------
	BoxMotifSet(const double &warp_in = 1.0, const double &scale_in = 1.0, const size_t &length_in = 100, const double &height_in = 50.0);
};

// ----------------------------------------------------------------------------
///\brief This class represents the triangle motif set.
///
/// The TriangleMotifSet looks similiar to:
/// \code
/// ^
/// |
/// |          .
/// |        .   .
/// |      .       .
/// |    .           .
/// | ..               ..
/// O---------------------->
/// \endcode
// ----------------------------------------------------------------------------
class TriangleMotifSet : public BasicMotifSet {

public:

    // ----------------------------------------------------------------------------
    ///\brief This is the triangle motif set definition.
    ///
    ///\param [in] &warp_in Hands over the warp of the triangle motif set.
    ///\param [in] &scale_in Hands over the scale of the triangle motif set.
    ///\param [in] &length_in Hands over the length of the triangle motif set.
    ///\param [in] &height_in Hands over the heigth of the triangle motif set.
    ///
    ///This is the triangle motif set definition. The motif set term warp x length has to be at least 3. Otherwise, the motif set is a two value line.
    // ----------------------------------------------------------------------------
	TriangleMotifSet(const double &warp_in = 1.0, const double &scale_in = 1.0, const size_t &length_in = 100, const double &height_in = 50.0);
};

// ----------------------------------------------------------------------------
///\brief This class represents the semicircle motif set.
///
/// The SemicircleMotifSet looks similiar to:
/// \code
/// ^
/// |
/// |       .......
/// |    .           .
/// |   .             .
/// | 
/// | ..               ..
/// O---------------------->
/// \endcode
// ----------------------------------------------------------------------------
class SemicircleMotifSet : public BasicMotifSet {

public:

    // ----------------------------------------------------------------------------
    ///\brief This is the semicircle motif set definition.
    ///
    ///\param [in] &warp_in Hands over the warp of the semicircle motif set.
    ///\param [in] &scale_in Hands over the scale of the semicircle motif set.
    ///\param [in] &length_in Hands over the length of the semicircle motif set.
    ///\param [in] &height_in Hands over the heigth of the semicircle motif set.
    ///
    ///This is the semicircle motif set definition. The motif set term warp x length has to be at least 3. Otherwise, the motif set is a two value line.
    // ----------------------------------------------------------------------------
	SemicircleMotifSet(const double &warp_in = 1.0, const double &scale_in = 1.0, const size_t &length_in = 100, const double &height_in = 50.0);
};

// ----------------------------------------------------------------------------
///\brief This class represents the trapezoid motif set.
///
/// The TrapezoidMotifSet looks similiar to:
/// \code
/// ^
/// |
/// |      .........
/// |     .         .
/// |    .           .
/// |   .             .
/// | ..               ..
/// O---------------------->
/// \endcode
// ----------------------------------------------------------------------------
class TrapezoidMotifSet : public BasicMotifSet {

public:

    // ----------------------------------------------------------------------------
    ///\brief This is the trapezoid motif set definition.
    ///
    ///\param [in] &warp_in Hands over the warp of the trapezoid motif set.
    ///\param [in] &scale_in Hands over the scale of the trapezoid motif set.
    ///\param [in] &length_in Hands over the length of the trapezoid motif set.
    ///\param [in] &height_in Hands over the heigth of the trapezoid motif set.
    ///
    ///This is the trapezoid motif set definition. The motif set term warp x length has to be at least 3. Otherwise, the motif set is a two value line.
    // ----------------------------------------------------------------------------
	TrapezoidMotifSet(const double &warp_in = 1.0, const double &scale_in = 1.0, const size_t &length_in = 100, const double &height_in = 50.0);
};

// ----------------------------------------------------------------------------
///\brief This class represents the positive flank motif set.
///
/// The PositiveFlankMotifSet looks similiar to:
/// \code
/// ^
/// |
/// |                 .
/// |             .
/// |         .
/// |     .
/// | ..               ..
/// O---------------------->
/// \endcode
// ----------------------------------------------------------------------------
class PositiveFlankMotifSet : public BasicMotifSet {

public:

    // ----------------------------------------------------------------------------
    ///\brief This is the positive flank motif set definition.
    ///
    ///\param [in] &warp_in Hands over the warp of the positive flank motif set.
    ///\param [in] &scale_in Hands over the scale of the positive flank motif set.
    ///\param [in] &length_in Hands over the length of the positive flank motif set.
    ///\param [in] &height_in Hands over the heigth of the positive flank motif set.
    ///
    ///This is the positive flank motif set definition. The motif set term warp x length has to be at least 3. Otherwise, the motif set is a two value line.
    // ----------------------------------------------------------------------------
	PositiveFlankMotifSet(const double &warp_in = 1.0, const double &scale_in = 1.0, const size_t &length_in = 100, const double &height_in = 50.0);
};

// ----------------------------------------------------------------------------
///\brief This class represents the negative flank motif set.
///
/// The NegetiveFlankMotifSet looks similiar to:
/// \code
/// ^
/// |
/// |   .
/// |       .
/// |           .
/// |               .
/// | ..               ..
/// O---------------------->
/// \endcode
// ----------------------------------------------------------------------------
class NegativeFlankMotifSet : public BasicMotifSet {

public:

    // ----------------------------------------------------------------------------
    ///\brief This is the negative flank motif set definition.
    ///
    ///\param [in] &warp_in Hands over the warp of the negative flank motif set.
    ///\param [in] &scale_in Hands over the scale of the negative flank motif set.
    ///\param [in] &length_in Hands over the length of the negative flank motif set.
    ///\param [in] &height_in Hands over the heigth of the negative flank motif set.
    ///
    ///This is the negative motif set definition. The motif set term warp x length has to be at least 3. Otherwise, the motif set is a two value line.
    // ----------------------------------------------------------------------------
	NegativeFlankMotifSet(const double &warp_in = 1.0, const double &scale_in = 1.0, const size_t &length_in = 100, const double &height_in = 50.0);
};

// ----------------------------------------------------------------------------
///\brief This class represents the sine motif set.
///
/// The SineMotifSet looks similiar to:
/// \code
/// ^
/// |     . .
/// |   .     .
/// |  .       
/// O-.--------.--------.-->
/// |                  .
/// |           .     .
/// |             . .
/// \endcode
// ----------------------------------------------------------------------------
class SineMotifSet : public BasicMotifSet {

public:

    // ----------------------------------------------------------------------------
    ///\brief This is the sine motif set definition.
    ///
    ///\param [in] &warp_in Hands over the warp of the sine motif set.
    ///\param [in] &scale_in Hands over the scale of the sine motif set.
    ///\param [in] &length_in Hands over the length of the sine motif set.
    ///\param [in] &height_in Hands over the heigth of the sine motif set.
    ///
    ///This is the sine motif set definition. The motif set term warp x length has to be at least 3. Otherwise, the motif set is a two value line.
    // ----------------------------------------------------------------------------
	SineMotifSet(const double &warp_in = 1.0, const double &scale_in = 1.0, const size_t &length_in = 100, const double &height_in = 50.0);
};

// ----------------------------------------------------------------------------
///\brief This class represents the sine motif set.
///
///The CosineMotifSet looks similiar to:
/// \code
/// ^
/// O--.---------------.--->
/// |    .           .
/// |              
/// |     .         .
/// |              
/// |       .     .
/// |         . .
/// \endcode
// ----------------------------------------------------------------------------
class CosineMotifSet : public BasicMotifSet {

public:

    // ----------------------------------------------------------------------------
    ///\brief This is the cosine motif set definition.
    ///
    ///\param [in] &warp_in Hands over the warp of the cosine motif set.
    ///\param [in] &scale_in Hands over the scale of the cosine motif set.
    ///\param [in] &length_in Hands over the length of the cosine motif set.
    ///\param [in] &height_in Hands over the heigth of the cosine motif set.
    ///
    ///This is the cosine motif set definition. The motif set term warp x length has to be at least 3. Otherwise, the motif set is a two value line.
    // ----------------------------------------------------------------------------
	CosineMotifSet(const double &warp_in = 1.0, const double &scale_in = 1.0, const size_t &length_in = 100, const double &height_in = 50.0);
};

#endif
