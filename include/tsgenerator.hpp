///\file tsgenerator.hpp
///
///\brief File contains the TSGenerator class declaration.
///
///This is the header file of the TSGenerator. The TSGenerator consists of setter functions and a run function. After configuring the TSGenerator with
///the setter functions one starts the time series generation by calling the run function.
///

#ifndef TSGENERATOR_HPP
#define TSGENERATOR_HPP
#include <fstream>
#include <iostream>
#include <basicmotifset.hpp>
#include <motifsetcollection.hpp>
#include <mk.hpp>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <string>
#include <random>
#include <chrono>
#include <cmath>
#include <cfloat>
#include <limits>
#include <iterator>
#include <vector>


using namespace std;


extern bool check_if_float(const string string_in);

// ----------------------------------------------------------------------------
///\brief This class represents the TSGenerator.
///
///The TSGenerator consists of setter functions and a run function as well as various variables. After configuring the TSGenerator with the setter functions
///one starts the time series generation by calling the run function.
// ----------------------------------------------------------------------------
class TSGenerator {

public:

	// ----------------------------------------------------------------------------
	///\brief This struct contains the motif set key data.
	///
	///The motif set struct is used as a container to hand over the informations of a motif set.
	// ----------------------------------------------------------------------------
	struct motifSet {

		size_t type = 0;
		size_t amount = 0;
		double height = 0.0;
		double d = 0;
	};

	// ----------------------------------------------------------------------------
	///\brief This struct contains a free time series subsequence.
	///
	///The free time series position contains a free time series subsequence without a motif set subsequence.
	// ----------------------------------------------------------------------------
	struct freeTimeSeriesPosition {

		size_t position = 0;
		size_t length = 0;
	};
    
protected:
	// ----------------------------------------------------------------------------
	///\brief Is set true, if a true random engine is avaible on the system.
	///
	///This variable is set on true during initialization, if a true random engine is avaible on the system. Otherwise, it is set on false.
	///
	// ----------------------------------------------------------------------------
	bool trueRandomEngineAvailable;
	
	// ----------------------------------------------------------------------------
	///\brief True random device on the system.
	///
	///This variable stores the pointer of the true random engine avaible on the system. This variable is ignored, if there is not a random engine available on
	///the system.
	// ----------------------------------------------------------------------------
	random_device randomDevice;

	// ----------------------------------------------------------------------------
	///\brief This variable contains the base value for the time series values.
	///
	///This variable stores the base value of the time series values. The time series is generated around this value.
	// ----------------------------------------------------------------------------
	double baseValue = 0.0;
    
	// ----------------------------------------------------------------------------
	///\brief This variable contains the length of the time series.
	///
	///The time series has the default length 10000, if this variable is set to zero. Otherwise, the time series has a random length, if the
	///variable length is set to -1.
	// ----------------------------------------------------------------------------
	size_t length = 10000;

	// ----------------------------------------------------------------------------
	///\brief This variable the window size.
	///
	///The window size is used to generate the motif sets. The default value is 100.
	// ----------------------------------------------------------------------------
	size_t windowSize = 100;

	// ----------------------------------------------------------------------------
	///\brief This variable contains the noise ratio for the time series.
	///
	///This variable stores the noise ratio for the time series. This value is used for random noise calculation and has to be positive.
	// ----------------------------------------------------------------------------
	double noiseRatio = 0.1;

	// ----------------------------------------------------------------------------
	///\brief This list contains all motif types.
	///
	///This variable stores the names of all motifs available in this implementation. To add a motif one has to declare the motif in the
	///motifcollection.hpp file and define the motif in the motifcollection.cpp file as well. Also, one has to modify the addMotif() function in the file
	///tsgenerator.cpp.
	// ----------------------------------------------------------------------------
	vector<string> definedMotifSetTypes{"box", "triangle", "semicircle", "trapezoid", "positiveflank", "negativeflank", "sine", "cosine"};

	// ----------------------------------------------------------------------------
	///\brief This variable contains the lower bound of the definition area.
	///
	///This variable stores the parsed lower bound of the definition area.
	// ----------------------------------------------------------------------------
	double xOne = -200.0;

	// ----------------------------------------------------------------------------
	///\brief This variable contains the upper bound of the definition area.
	///
	///This variable stores the parsed upper bound of the definition area.
	// ----------------------------------------------------------------------------
	double xTwo = 200.0;

	// ----------------------------------------------------------------------------
	///\brief This variable contains the parsed motif sets.
	///
	///This variable stores the parsed motif sets that will be added to the time series.
	// ----------------------------------------------------------------------------
	vector<motifSet> motifSetVector;

	// ----------------------------------------------------------------------------
	///\brief This variable contains the free positions.
	///
	///This variable stores the positions of free subsequences in the time series between the motif sets subsequences.
	// ----------------------------------------------------------------------------
	vector<freeTimeSeriesPosition> freePositions;


	// ----------------------------------------------------------------------------
	///\brief Checks the parsed arguments.
	///
	///This function checks the parsed arguments for consistency.
	// ----------------------------------------------------------------------------
	void checkParsedArguments();

	// ----------------------------------------------------------------------------
	///\brief Calculates the similarity of two time series.
	///
	///\param [in] &timeSeriesOne_in Hands over the first time series.
	///\param [in] &timeSeriesTwo_in Hands over the second time series.
	///\param [in] bestSoFar_in Hands over the best similarity so far.
	///
	///\return The similarity of the two z-normalized time series.
	///
	///This function calculates the similarity of two time series. Therefore, the time series are first z-normalized and the
	///Euclidean Distance is calculated. The return value is the similarity of the two z-normalized time series.
	// ----------------------------------------------------------------------------
	double similarity(const vector<double> &timeSeriesOne_in, const vector<double> &timeSeriesTwo_in, double bestSoFar_in = numeric_limits<double>::max());

	// ----------------------------------------------------------------------------
	///\brief Calculates the next noise value.
	///
	///\param [in] &randomValue_in Hands over the distribution for generation of noise.
	///\param [in] &pseudoRandomEngine_in Hands over the pseudo random engine for generation of noise.
	///
	///\return Noise value that is added to the time series value.
	///
	///This function calculates the noise of the time series. The return value is added to the time series value by the time series generation functions.
	///Also, the function checks whether the random generated noise value is out of bound.
	// ----------------------------------------------------------------------------
	double calculateNoise(normal_distribution<double> &randomValue_in, mt19937 &pseudoRandomEngine_in);

	// ----------------------------------------------------------------------------
	///\brief Calculates a raw motif set subsequence.
	///
	///\param [out] subsequence_out Hands over the calculated raw subsequence.
	///\param [in] type_in Hands over the motif set type.
	///\param [in] height_in Hands over the height of the subsequence.
	///\param [in] windowSize_in Hands over the window size.
	///\param [in] noiseRatio_in Hands over the noise ratio.
	///
	///This function calculates a raw motif set subsequence according to the motif set type, height and window size. Attention! The range is random
	///and the subsequence is not added to the synthetic time series. The noise span is used to calculate the noise for the subsequence.
	// ----------------------------------------------------------------------------
	void calculateRawSubsequence(vector<double> &subsequence_out, size_t type_in, size_t windowSize_in, double height_in, double noiseRatio_in);

	// ----------------------------------------------------------------------------
	///\brief Calculates the range of all subsequences.
	///
	///\param [in] &subsequence_in Hands over the new subsequence.
	///\param [in] &subsequences_in Hands over all other subsequences.
	///
	///\return The range of all subsequences.
	///
	///This function calculates the range of all subsequences to each other.
	// ----------------------------------------------------------------------------
	double calculateRange(const vector<double> &subsequence_in, const vector<vector<double>> &subsequences_in);

	// ----------------------------------------------------------------------------
	///\brief Calculates a random free position in the time series.
	///
	///\param [in] &freePositions_in Hands over the free positions in the time series as subsequences.
	///\param [in] &windowSize_in Hands over the window size.
	///
	///\return The random position in the time series.
	///
	///This function calculates a random free position in the time series according to the window size.
	// ----------------------------------------------------------------------------
	size_t calculateRandomPosition(vector<freeTimeSeriesPosition> &freePositions_in, size_t windowSize_in);

	// ----------------------------------------------------------------------------
	///\brief Add noise to subsequence.
	///
	///\param [in,out] &subsequence Hands over the subsequence.
	///\param [in] noiseRatio_in Hands over the noise ratio.
	///
	///This function adds noise to a subsequence according to the noise ratio.
	// ----------------------------------------------------------------------------
	void addNoiseToSubsequence(vector<double> &subsequence, double noiseRatio_in);

	// ----------------------------------------------------------------------------
	///\brief Generates a random syntheteic time series.
	///
	///\param [out] &timeSeries_in Hands over the random synthetic time series.
	///
	///This function generates a random synthetic time series by doing the random walk. The first 100 random generated values are ignored.
	// ----------------------------------------------------------------------------
	void generateRandomTS(vector<double> &timeSeries_out);

	// ----------------------------------------------------------------------------
	///\brief Search for unintentional matches in the time series with new injected subsequence.
	///
	///\param [in] &timeSereis_in Hands over the time series.
	///\param [in] &subsequencePositions_in Hands over the position of the new subsequence.
	///\param [in] similarity_in Hands over the similarity to break.
	///
	///\return true if there exists an unintentional subsequence match with the new subequence.
	///
	///This function searches for unintentinal matches of subsequences in the time series with the new injected subsequence.
	// ----------------------------------------------------------------------------
	bool searchForUnintentionalMatches(const vector<double> &timeSeries_in, const vector<size_t> &motifSetPositions_in, double similarity_in);

public:

	// ----------------------------------------------------------------------------
	///\brief The constructor initializes the TSGenerator.
	///
	///The constructor checks whether a true random engine is available and stores the result in the trueRandomEngineAvailable variable.
	// ----------------------------------------------------------------------------
	TSGenerator();

	// ----------------------------------------------------------------------------
	///\brief Frees the memory allocated by the TSGenerator.
	///
	///The destructor does actually nothing. 
	// ----------------------------------------------------------------------------
    ~TSGenerator();

	// ----------------------------------------------------------------------------
	///\brief Generates a time series with defined time series motif sets.
	///
	///\param [out] &timeSeries_in Hands over the time series.
	///\param [out] &d_in Hands over the range of each the motif sets.
	///\param [out] &windowSize_out Hands over the window size of each motif set.
	///\param [out] &motifSetPositions_in Hands over the positions of each motif set.
	///\param [in]  type_in Hands over the time series top motif pair subsequence type.
	///\param [in]  height_in Hands over the time series top motif pair subsequence height.
	///
	///First, this function checks all variables and pointers for validity. Some random time series values are forwarded. Afterwards, the time series is generated and
	///the time series and the motif positions are written into a file. A not defined motif tag is treated as a random motif. Therfore, a ranodm motif type is chosen.
	///The default motif type is the random motif.
	// ----------------------------------------------------------------------------
	void runMotifPair(vector<double> &timeSeries_out, vector<double> &d_out, vector<size_t> &windowSize_out, vector<vector<size_t>> &motifSetPositions_out,
					  const string type_in, double height_in);

	// ----------------------------------------------------------------------------
	///\brief Generates a time series with defined time series motif sets.
	///
	///\param [out] &timeSeries_in Hands over the time series.
	///\param [out] &d_in Hands over the range of each the motif sets.
	///\param [out] &windowSize_out Hands over the window size of each motif set.
	///\param [out] &motifSetPositions_in Hands over the positions of each motif set.
	///
	///First, this function checks all variables and pointers for validity. Some random time series values are forwarded. Afterwards, the time series is generated and
	///the time series and the motif positions are written into a file. A not defined motif tag is treated as a random motif. Therfore, a ranodm motif type is chosen.
	///The default motif type is the random motif.
	// ----------------------------------------------------------------------------
	void runMotifSets(vector<double> &timeSeries_out, vector<double> &d_out, vector<size_t> &windowSize_out, vector<vector<size_t>> &motifSetPositions_out);

	// ----------------------------------------------------------------------------
	///\brief This is the noise ratio setter function.
	///
	///\param [in] &noiseRatio_in Hands over the noise ratio.
	///
	///This function sets the noise ratio used to add noise to the time series. The noise ratio has to be positive. Otherwise, it is set to zero.
	// ----------------------------------------------------------------------------
	void setRandomness(const double &noiseRatio_in);

	// ----------------------------------------------------------------------------
	///\brief This is the range setter function of the time series values.
	///
	///\param [in] &xOne_in Hands over the first range value.
	///\param [in] &xTwo_in Hands over the second range value.
	///
	///This function sets the range of the time series values. The value xOne_in and xTwo_in are the range bounds.
	///The bounds are used for the time series value generation. The bounds difference have to be great enough and xOne is set to the
	///lower bound.
	// ----------------------------------------------------------------------------
	void setRange(const double &xOne_in, const double &xTwo_in);

	// ----------------------------------------------------------------------------
	///\brief This is the time series length setter function.
	///
	///\param [in] length_in Hands over the time series length.
	///
	///This function sets the time series length. The length has to be at least 100.
	// ----------------------------------------------------------------------------
	void setLength(size_t length_in);

	// ----------------------------------------------------------------------------
	///\brief This is the window size setter function.
	///
	///\param [in] windowSize_in Hands over the window size.
	///
	///This function sets the window size. The window size is used to generate the subsequences for the motif sets.
	// ----------------------------------------------------------------------------
	void setWindowSize(size_t windowSize_in);

	// ----------------------------------------------------------------------------
	///\brief This is the motif set vector setter function.
	///
	///\param [in] &motifSetVector_in Hands over the motif set vector.
	///
	///This function sets the motif set vector of the time series. First the motif set vector is emptyed. Repeatedly the motif sets types are checked
	///checked for correctness. If random is set as type a random motif set type is chosen. After, the motif set size and length are checked for
	///correctness and set. If any parameter is wrong set the program ends with an error.
	// ----------------------------------------------------------------------------
	void setMotifSetVector(const vector<string> &motifSetVector_in);
};

#endif
