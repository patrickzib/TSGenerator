///\file basicmotifset.cpp
///
///\brief File contains the basic motif set class definition.
///
///This is the source file of the basic motif set. The basic motif set consists of values in a list and an iterator on the next value
///that will be returned. Also, the basic motif set stroes the warp, scale, length and maximal height of the motif set.

#include <basicmotifset.hpp>

BasicMotifSet::BasicMotifSet() {}

double BasicMotifSet::getLastValue() {

	return *(values.end());
}

void BasicMotifSet::getSequence(vector<double> &subsequence_out) {
	
	for (size_t itr = 0; itr < values.size(); itr++)
		subsequence_out.push_back(values.at(itr));
}

void BasicMotifSet::addNextValue(double value_in) {

	values.push_back(value_in);
}
