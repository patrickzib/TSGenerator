﻿// ----------------------------------------------------------------------------
///\file main.cpp
///
///\brief Here starts the TSGenerator.
///
///This is the main.cpp file of the TSGenerator. The program uses a configuration file for the time series configuration.
// ----------------------------------------------------------------------------

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <list>
#include <tsgenerator.hpp>
#include <outputgenerator.hpp>

using namespace std;

// ----------------------------------------------------------------------------
///\brief Defines the program name.
///
///Makro that defines the name of the program.
// ----------------------------------------------------------------------------
#define PROGNAME "TSGenerator"

// ----------------------------------------------------------------------------
///\brief Defines the program version.
///
///Makro that defines the version of the program.
// ----------------------------------------------------------------------------
#define VERSION  "2.6.0.1"

// ----------------------------------------------------------------------------
///\brief Checks if string represents float.
///
///\param [in] string_in The string to be checked.
///
///This function checks if the string is a float. Float representation of form [-](123456789)+[.(123456789)+] are accepted.
// ----------------------------------------------------------------------------
bool check_if_float(const string string_in) {

	bool isFloat = false;
	size_t stringItr = 0;

	if (string_in.at(stringItr) == '-')
		stringItr++;

	if (stringItr < string_in.length() && '0' <= string_in.at(stringItr) && string_in.at(stringItr) <= '9') {

		while (stringItr < string_in.length() && '0' <= string_in.at(stringItr) && string_in.at(stringItr) <= '9')
			stringItr++;

		if (stringItr == string_in.length())
			isFloat = true;
		else {

			if (string_in.at(stringItr) == '.') {

				stringItr++;

				if (stringItr < string_in.length() && '0' <= string_in.at(stringItr) && string_in.at(stringItr) <= '9') {

					while (stringItr < string_in.length() && '0' <= string_in.at(stringItr) && string_in.at(stringItr) <= '9')
						stringItr++;

					if (stringItr == string_in.length())
						isFloat = true;
				}
			}
		}
	}

	return isFloat;
}

// ----------------------------------------------------------------------------
///\brief Parses arguments into a vector.
///
///\param [in] argc Hands over the number of arguments.
///\param [in] **argv Hands over the arguments.
///\param [out] *argTokens Returns the parsed arguments vector.
///
///This is a argument parser function that parses arguments into a string vector.
// ----------------------------------------------------------------------------
void parseArgs(int argc, char **argv, vector<string> &argTokens) {
    
    for (int i = 0; i < argc; i++)
        argTokens.push_back(string(argv[i]));
}

// ----------------------------------------------------------------------------
///\brief Checks whether argument is contained in argument vector or not.
///
///\param [in] *argTokens Hands over the parsed arguments vector.
///\param [in] arg_in Hands over the argument token.
///\param [out] *payload_out Returns the parsed payload .
///
///Function that checks whether argument is contained in the parsed argument vector or not.
///Additional it returns the payload, if available. Returns true, if the argument
///was found and sets the payload string. Otherwise the function returns false.
// ----------------------------------------------------------------------------
bool checkArg(vector<string> &argTokens, const string arg_in, vector<string> &payload_out) {

	if (!payload_out.empty()) {

		payload_out.clear();
		payload_out.resize(0);
	}

	vector<string>::iterator payload = find(argTokens.begin(), argTokens.end(), arg_in);

	if (payload == argTokens.end())
		return false;

	payload++;

	if (payload == argTokens.end() || ((*payload).front() == '-' && !check_if_float(*payload)))
		payload_out.push_back("");

	while (payload != argTokens.end() && ((*payload).front() != '-' || check_if_float(*payload))) {

		size_t position = 0;

		while ((position = (*payload).find(" ")) != string::npos) {

			payload_out.push_back((*payload).substr(0, position));

			(*payload).erase(0, position + 1);
		}

		payload_out.push_back(*payload);

		payload++;
	}

	return true;
}

// ----------------------------------------------------------------------------
///\brief Prints out the version text.
///
///A text containing the program name and version as well as additional informations like the authors name.
// ----------------------------------------------------------------------------
void print_version() {
    
    cout << PROGNAME << " " << VERSION << endl;

#ifdef _WIN32
	SetConsoleOutputCP(CP_UTF8);
	cout << "Copyright " << u8"©" << " 2018 Rafael Moczalla" << endl;
#else
    cout << "Copyright © 2018 Rafael Moczalla" << endl;
#endif
    cout << "Lizenz: Creative Commons - Attribution - Non-Commercial - Share Alike" << endl;
    cout << "There are no guarantees as far as the law permits." << endl << endl;
    cout << "Written by Rafael Moczalla" << endl;
}

// ----------------------------------------------------------------------------
///\brief Prints out the help text.
///
///A text containing the information about the use and the arguments of the TSGenerator.
// ----------------------------------------------------------------------------
void print_help() {
    
	cout << "Call: " << endl;
	cout << "    " << PROGNAME << " -l INTEGER -w INTEGER [Options]" << endl;
	cout << endl;
	cout << "    -l,    --length INTEGER                                  Sets the length of the time series." << endl;
	cout << "    -w,    --windowSize INTEGER                              Sets the window size of the time series." << endl;
	cout << endl;
    cout << "Options:" << endl;
	cout << "    -mp,   --motifPair STRING FLOAT                          Sets the operating mode to motif pair and" << endl;
	cout << "                                                             the subsequences type as well as height." << endl;
	cout << "                                                             The default operating mode is the motif" << endl;
	cout << "                                                             pair mode, the default type is BOX and the" << endl;
	cout << "                                                             default height is 200." << endl;
	cout << "    -msl,  --motifSetList (STRING INTEGER FLOAT)+            Sets the operating mode to motif sets and" << endl;
	cout << "                                                             the parameters for each motif set namely" << endl;
	cout << "                                                             the type, count and height." << endl;
	cout << "    -o,    --out NAME                                        Sets the output file name." << endl;
	cout << "    -tsn,  --timeSeriesName NAME                             Sets the time series name." << endl;
	cout << "    -ho,   --horizontalOutput                                Sets the output mode to horizontal." << endl;
	cout << "    -r,    --range FLOAT FLOAT                               Sets the range of the time series values." << endl;
	cout << "    -rd,   --randomness FLOAT                                Sets the randomness of the time series." << endl;
	cout << "    -h,    --help                                            Prints these help text." << endl;
	cout << "    -v,    --version                                         Prints the version information." << endl;
}

// ----------------------------------------------------------------------------
///\brief Function called at program start.
///
///\param [in] argc Hands over the number of arguments to main.
///\param [in] **argv Hands over the arguments to main.
///
///This is the typical main function of C++. The main function is called at the program start. First all
///non-local objects with static storage duration are initialized. We use the main function to parse the argument list to set up
///and to start the TSGenerator. Also a configuration file is created, if a configuration file do not exist.
// ----------------------------------------------------------------------------
int main(int argc, char *argv[]) {

    vector<string> argTokens;
    vector<string> payload;
    
    parseArgs(argc, argv, argTokens);
    
    if (checkArg(argTokens, "-v", payload) || checkArg(argTokens, "--version", payload)) {
        
        print_version();
        exit(EXIT_SUCCESS);
    }
    
    if (checkArg(argTokens, "-h", payload) || checkArg(argTokens, "--help", payload) || argc < 2) {
        
        print_help();
        exit(EXIT_SUCCESS);
    }

	if (argc > 1                                        &&
		!checkArg(argTokens, "-l", payload)             &&
		!checkArg(argTokens, "--length", payload)       &&
		!checkArg(argTokens, "-w", payload)             &&
		!checkArg(argTokens, "--windowSize", payload)   &&
		!checkArg(argTokens, "-mp", payload) &&
		!checkArg(argTokens, "--motifPair", payload) &&
		!checkArg(argTokens, "-msl", payload)           &&
		!checkArg(argTokens, "--motifSetList", payload)   ) {

		cerr << "ERROR: Missing required arguments!" << endl;
		exit(EXIT_FAILURE);
	}

	if (argc > 1 &&
		(checkArg(argTokens, "-mp", payload) ||
		 checkArg(argTokens, "--motifPair", payload)) &&
		(checkArg(argTokens, "-msl", payload) ||
		 checkArg(argTokens, "--motifSetList", payload))) {

		cerr << "ERROR: Choose motif pair xor motif operating mode!" << endl;
		exit(EXIT_FAILURE);
	}

	vector<double> timeSeries;
	vector<double> dVector;
	vector<size_t> windowSizes;
	vector<vector<size_t>> motifSetPositions;
	TSGenerator tsGenerator;

#ifndef TEST_MODE

	if (checkArg(argTokens, "-l", payload) || checkArg(argTokens, "--length", payload)) {

		if (payload.at(0).compare("") == 0) {

			cerr << "ERROR: Length is missing an argument." << endl;
			exit(EXIT_FAILURE);
		}

		if (payload.at(0).find_first_not_of("0123456789") != string::npos) {

			cerr << "ERROR: " << (payload.at(0)) << " is not a valid number!" << endl;
			exit(EXIT_FAILURE);
		}

		tsGenerator.setLength(stoll(payload.at(0)));
	}

	if (checkArg(argTokens, "-w", payload) || checkArg(argTokens, "--windowSize", payload)) {

		if (payload.at(0).compare("") == 0) {

			cerr << "ERROR: Window size is missing an argument." << endl;
			exit(EXIT_FAILURE);
		}

		if (payload.at(0).find_first_not_of("0123456789") != string::npos) {

			cerr << "ERROR: " << (payload.at(0)) << " is not a valid number!" << endl;
			exit(EXIT_FAILURE);
		}

		tsGenerator.setWindowSize(stoll(payload.at(0)));
	}

	if (checkArg(argTokens, "-r", payload) || checkArg(argTokens, "--range", payload)) {

		double xOne;
		double xTwo;

		vector<string>::iterator payloadItr = payload.begin();

		if ((*payloadItr).compare("") == 0) {

			cerr << "ERROR: Range is missing arguments." << endl;
			exit(EXIT_FAILURE);
		}

		if (!check_if_float(*payloadItr)) {

			cerr << "ERROR: Range bound x_1 is not a float." << endl;
			exit(EXIT_FAILURE);
		}

		xOne = stod(*payloadItr);


		payloadItr++;

		if ((*payloadItr).compare("") == 0) {

			cerr << "ERROR: Range is missing an argument." << endl;
			exit(EXIT_FAILURE);
		}

		if (!check_if_float(*payloadItr)) {

			cerr << "ERROR: Range bound x_2 is not a float." << endl;
			exit(EXIT_FAILURE);
		}

		xTwo = stod(*payloadItr);

		tsGenerator.setRange(xOne, xTwo);
	}

	if (checkArg(argTokens, "-rd", payload) || checkArg(argTokens, "--randomness", payload)) {

		if (payload.at(0).compare("") == 0) {

			cerr << "ERROR: Randomness is missing an argument." << endl;
			exit(EXIT_FAILURE);
		}

		if (!check_if_float(payload.at(0))) {

			cerr << "ERROR: Randomness bound x_2 is not a float." << endl;
			exit(EXIT_FAILURE);
		}

		tsGenerator.setRandomness(stold(payload.at(0)));
	}

	if (checkArg(argTokens, "-msl", payload) || checkArg(argTokens, "--motifSetList", payload)) {
		
		tsGenerator.setMotifSetVector(payload);
		tsGenerator.runMotifSets(timeSeries, dVector, windowSizes, motifSetPositions);
	}
	else {

		if (checkArg(argTokens, "-mp", payload) || checkArg(argTokens, "--motifPair", payload)) {
			
			if (payload.size() != 2) {

				cerr << "ERROR: Wrong number of arguments in motif pair option." << endl;
				exit(EXIT_FAILURE);
			}

			if (!check_if_float(payload.at(1))) {

				cerr << "ERROR: " << payload.at(1) <<" is not a float." << endl;
				exit(EXIT_FAILURE);
			}
				
			tsGenerator.runMotifPair(timeSeries, dVector, windowSizes, motifSetPositions, payload.at(0), stold(payload.at(1)));
		}
		else
			tsGenerator.runMotifPair(timeSeries, dVector, windowSizes, motifSetPositions, "box", 200.0);
	}


	OutputGenerator *outputFile = nullptr;

	if (checkArg(argTokens, "-o", payload) || checkArg(argTokens, "--out", payload)) {

		if (payload.at(0).compare("") == 0) {

			cerr << "ERROR: The output file name is unset." << endl;
			exit(EXIT_FAILURE);
		}
		else
			outputFile = new OutputGenerator(payload.at(0));
	}
	else
		outputFile = new OutputGenerator();

	if (checkArg(argTokens, "-tsn", payload) || checkArg(argTokens, "--timeSeriesName", payload)) {

		if (payload.at(0).compare("") == 0) {

			cerr << "ERROR: The time series name is unset." << endl;
			exit(EXIT_FAILURE);
		}
		else
			outputFile->setTimeSeriesName(payload.at(0));
	}

	if (checkArg(argTokens, "-ho", payload) || checkArg(argTokens, "--horizontalOutput", payload))
		outputFile->printTimeSeriesHorizontal(timeSeries, dVector, windowSizes, motifSetPositions);
	else
		outputFile->printTimeSeriesVertical(timeSeries, dVector, windowSizes, motifSetPositions);

#endif

    exit(EXIT_SUCCESS);
}
