/** \file mk.cpp
 * 
 * \brief File contains the mk algorithm written by Abdullah Al Mueen.
 * 
 * This is the source file containing the mk algorithm written by Abdullah Al Mueen at the department of
 * Computer Science and Engineering of University of California - RIverside. The algorithm is based on the
 * paper EXACT DISCOVERY OF TIME SERIES MOTIFS by Abdullah Mueen, Eamonn Keogh and Qiang Zhu.
 */

#include <mk.hpp>


vector<vector<double>> indices;

int refMax;                                                                        //reference point that has maximum std
vector<double> stdRef;                                                                 //Standard Deviations of all the reference points

/*Calculates the distance between two time series x and y. If the distance is
larger than the best so far (bsf) it stops computing and returns the approximate
distance. To get exact distance the bsf argument should be omitted.*/

double distance(const vector<double> &x_in, const vector<double> &y_in, size_t length_in, double best_so_far) {

    size_t i;
    double sum = 0;
    double bsf2 = best_so_far*best_so_far;
    for ( i = 0 ; i < length_in && sum < bsf2 ; i++ )
        sum += (x_in[i]-y_in[i])*(x_in[i]-y_in[i]);
    return sqrt(sum);
    }

/*Comparison function for qsort function. Compares two time series by using their
distances from the reference time series. */

bool comp1(const long long a, const long long b) {

	long long x = a;
	long long y = b;

	if (indices[refMax][x] > indices[refMax][y])
		return true;
	else
		return false;
}

bool comp2(const int a, const int b) {

	int x = a;
	int y = b;

	if (stdRef[x]<stdRef[y])
		return true;
	else
		return false;
}
    
double mk(const vector<double> &timeSeries_in, size_t windowSize_in, size_t &positionOne_out, size_t &positionTwo_out)
{
	vector<vector<double>> mkData;
	vector<vector<double>> dref;
	vector<long long> ind;                                                                 //The sorted indices (I-ordering) to the time series in order of their distances to ref
	vector<int> rInd;                                                                      //The sorted indices (Z-ordering) to the reference points in order of their std of distances

	long long TIMESERIES;                                                           //The length of the Long Time Series.
	int LENGTH;                                                                     //Length of each time series
	int MAXREF;                                                                     //Number of reference points to be used

	long long loc1, loc2;                                                          //location of the current best pair
	double bsf;                                                                     //best-so-far

    double d;
    long long i , j ;
	long long offset = 0;
	int abandon = 0 ,  r = 0;
    double ex , ex2 , mean, std;
	int clear = 0;
    vector<double> cntr;
    double t1,t2;
	bool verbos = false;
    double count = 0;

    /* taking input time series from the file in the data array and Normalizing
    them as well. */

    bsf = numeric_limits<double>::max();
    i = 0;
    j = 0;
    ex = ex2 = 0;

    LENGTH = windowSize_in;
    MAXREF = 10;
    clear = windowSize_in;

	if (verbos) {

		t1 = clock();
		printf("\nLength of the Time Series : %lld\nLength of Subsequences : %d\n\n", (long long)timeSeries_in.size(), LENGTH);
	}
	
    TIMESERIES = timeSeries_in.size() -LENGTH+1;

    mkData.resize(TIMESERIES);
    ind.resize(TIMESERIES);
	
    while(i < (long long)timeSeries_in.size())
    {
		d = timeSeries_in.at(i);
        
        if( i < TIMESERIES )
        {
            mkData[i].resize(LENGTH);
            ind[i] = i;
        }

        ex += d;
        ex2 += d*d;

		int k = LENGTH-1;
		for (j = ((i >= TIMESERIES) ? TIMESERIES - 1 : i); j >= ((i >= LENGTH) ? (i - k) : 0); j--)
			mkData[j][i - j] = d;

        if( i >= k )
        {
            mean = ex/LENGTH;
            std = ex2/LENGTH;
            std = sqrt(std-mean*mean);
            ex -= mkData[i-k][0];
            ex2 -= mkData[i-k][0]*mkData[i-k][0];
            for( int u = 0 ; u < LENGTH ; u++ )
                mkData[i-k][u] = (mkData[i-k][u]-mean)/std;
        }
            
        i++;
    }


    if(verbos)
        printf("Data Have been Read and Normalized\n\n");
    
    dref.resize(MAXREF);
    indices.resize(MAXREF);
	stdRef.resize(MAXREF);
    cntr.resize(MAXREF);
    rInd.resize(MAXREF);
    
    //////////////////////////////////////////////////////////////////////////


    /*Generating the reference time series. Here it is a random time series*/

    srand ( time(NULL) );

    for( r = 0 ; r < MAXREF ; r++ )
    {

		dref[r].resize(LENGTH);
		indices[r].resize(TIMESERIES);

		long long random_pick = rand() % TIMESERIES;
		for( i = 0 ; i < LENGTH ; i++ )
			dref[r][i] = mkData[random_pick][i];

		if(verbos)
			printf("\nThe %lldth subsequence is chosen as %dth reference\n",random_pick,r);


        /*Creating the indices array which is a 2 by TIMESERIES
        sized vector having the indices (to the data array) and distances (from the
        reference time series) in each row.*/


        ex = 0;
        ex2 = 0;
            

        for( i = 0 ; i < TIMESERIES ; i++ )
        {
            if( i == random_pick )
            { indices[r][i] = numeric_limits<double>::max(); continue; }
            d = indices[r][i] = distance(mkData[i],dref[r],LENGTH);
            count = count + 1;
            ex += d;
            ex2 += d*d;
            if ( abs( i - random_pick ) <= clear )  continue;
            if ( d < bsf )
            {
                bsf = d; loc1 = i; loc2 = random_pick;
                if(verbos)
                    printf("New best-so-far is %lf and (%lld , %lld) are the new motif pair\n",bsf,loc1,loc2);
                }

        }

        ex = ex/(TIMESERIES-1);
        ex2 = ex2/(TIMESERIES-1);
        std = sqrt(ex2-ex*ex);
            


        rInd[r] = r;
        stdRef[r] = std;
        cntr[r] = 0;
        //  printf("%d mean is %lf std is %lf \n",r,ex,std);
            ////////////////////////////////////////////////////////////////////
    }


    if(verbos)
        printf("\nReferences are picked and Dist has been Computed\n\n");
	
    /*Sort the standard Deviations*/
    sort(rInd.begin(),rInd.end(),comp2);

    refMax = rInd[0];

	long long remaining = TIMESERIES;


    /*Sort indices using the distances*/
    sort(ind.begin(),ind.end(),comp1);
    

    if(verbos)
        printf("Orderings have been Computed and Search has begun\n\n");

        /*Motif Search loop of the algorithm that finds the motif. The algorithm
    computes the distances between a pair of time series only when it thinks
    them as a potential motif'*/

    
    offset = 0;
    abandon = 0;
    
    while (!abandon && offset < remaining)
    {
        abandon = 1;
        offset++;

        for(i = 0 ; i < remaining - offset ; i++ )
        {
			long long left = ind[i];
			long long right = ind[i + offset];
            if( abs(left-right) <= clear )
                continue;

            //According to triangular inequality distances between left and right
            //is obviously greater than lower_bound.
            double lower_bound = 0;
            r = 0;
            do
            {
                lower_bound = fabs(indices[rInd[r]][right] - indices[rInd[r]][left]);
                r++;
            }while( r < MAXREF && lower_bound < bsf );

        
            if (r >= MAXREF && lower_bound < bsf)
            {

                abandon = 0;
                count =  count + 1;
                d = distance( mkData[left] , mkData[right] , LENGTH , bsf );
                
                if (d < bsf )
                {

                    bsf = d;
                    loc1 = left;
                    loc2 = right;
                    
                    if(verbos)
                        printf("New best-so-far is %lf and (%lld , %lld) are the new motif pair\n",bsf,loc1,loc2);

                    }
                }
            }
        }
	
	if (verbos) {

		printf("\n\nFinal Motif is the pair ( %lld", loc1);
		printf(", %lld ) and the Motif distance is %lf\n", loc2, bsf);
		t2 = clock();
		printf("\nExecution Time was : %lf seconds\n", (t2 - t1) / CLOCKS_PER_SEC);
	}
    
    positionOne_out = loc1;
	positionTwo_out = loc2;
    
	stdRef.clear();

    return bsf;
}


double mk_d(const vector<vector<double>> &timeSeriesDatabase_in, size_t windowSize_in)
{
	vector<vector<double>> mkData;
	vector<vector<double>> dref;
	vector<long long> ind;
	long long loc1, loc2;

	long long TIMESERIES;
	int LENGTH;
	int MAXREF;
	double bsf;

	double d;
	long long i, j;
	long long offset = 0;
	int abandon = 0, r = 0;
	double ex, ex2, mean, std;
	vector<int> rInd;
	int clear = 0;
	vector<double> cntr;
	double t1, t2;
	bool verbos = false;
	double count = 0;

	/* taking inpput time series from the file in the data array and Normalizing
	them as well. */

	bsf = numeric_limits<double>::max();
	i = 0;
	j = 0;
	ex = ex2 = 0;

	TIMESERIES = timeSeriesDatabase_in.size();
	LENGTH = windowSize_in;
	MAXREF = 10;

	if (verbos) {

		t1 = clock();
		printf("\nNumber of Time Series : %lld\nLength of Each Time Series : %d\n\n", TIMESERIES, LENGTH);
	}

	mkData.resize(TIMESERIES);
	ind.resize(TIMESERIES);

	mkData[0].resize(LENGTH);

	// printf("%ld %ld\n\n",data,data[0]);

	while (i < TIMESERIES)
	{
		d = mkData[i][j] = timeSeriesDatabase_in[i][j];
		ex += d;
		ex2 += d*d;
		if (j == LENGTH - 1)
		{
			mean = ex = ex / LENGTH;
			ex2 = ex2 / LENGTH;
			std = sqrt(ex2 - ex*ex);
			for (int k = 0; k < LENGTH; k++)
				mkData[i][k] = (mkData[i][k] - mean) / std;
			ex = ex2 = 0;
			ind[i] = i;
			i++;
			if (i != TIMESERIES)
				mkData[i].resize(LENGTH);

			j = 0;
		}
		else
			j++;
	}

	if (verbos)
		printf("Data Have been Read and Normalized\n\n");

	dref.resize(MAXREF);
	indices.resize(MAXREF);
	stdRef.resize(MAXREF);
	cntr.resize(MAXREF);
	rInd.resize(MAXREF);

	//////////////////////////////////////////////////////////////////////////

	/*Generating the reference time series. Here it is a random time series*/

	srand(time(NULL));

	for (r = 0; r < MAXREF; r++)
	{

		dref[r].resize(LENGTH);
		indices[r].resize(TIMESERIES);

		long long random_pick = rand() % TIMESERIES;
		for (i = 0; i < LENGTH; i++)
			dref[r][i] = mkData[random_pick][i];

		if (verbos)
			printf("\nThe %lldth Time Series is chosen as %dth reference\n", random_pick, r);

		/*Creating the indices array which is a 2 by TIMESERIES
		sized vector having the indices (to the data array) and distances (from the
		reference time series) in each row.*/


		ex = 0;
		ex2 = 0;


		for (i = 0; i < TIMESERIES; i++)
		{
			if (i == random_pick)
			{
				indices[r][i] = numeric_limits<double>::max(); continue;
			}
			d = indices[r][i] = distance(mkData[i], dref[r], LENGTH);
			count = count + 1;
			ex += d;
			ex2 += d*d;
			if (abs(i - random_pick) <= clear)  continue;
			if (d < bsf)
			{
				bsf = d; loc1 = i; loc2 = random_pick;
				if (verbos)
					printf("New best-so-far is %lf and (%lld , %lld) are the new motif pair\n", bsf, loc1, loc2);
			}

		}

		ex = ex / (TIMESERIES - 1);
		ex2 = ex2 / (TIMESERIES - 1);
		std = sqrt(ex2 - ex*ex);



		rInd[r] = r;
		stdRef[r] = std;
		cntr[r] = 0;
		////////////////////////////////////////////////////////////////////
	}

	if (verbos)
		printf("\nReferences are picked and Dist has been Computed\n\n");

	/*Sort the standard Deviations*/
	sort(rInd.begin(), rInd.end(), comp2);

	refMax = rInd[0];

	long long remaining = TIMESERIES;

	/*Sort indices using the distances*/
	sort(ind.begin(), ind.end(), comp1);
	///////////////////////////////////


	/*Motif Search loop of the algorithm that finds the motif. The algorithm
	computes the distances between a pair of time series only when it thinks
	them as a potential motif'*/

	if (verbos)
		printf("Orderings have been Computed and Search has begun\n\n");
	offset = 0;
	abandon = 0;

	while (!abandon && offset < remaining)
	{
		abandon = 1;
		offset++;

		for (i = 0; i < remaining - offset; i++)
		{
			long long left = ind[i];
			long long right = ind[i + offset];
			if (abs(left - right) <= clear)
				continue;

			//According to triangular inequality distances between left and right
			//is obviously greater than lower_bound.
			double lower_bound = 0;
			r = 0;
			do
			{
				lower_bound = fabs(indices[rInd[r]][right] - indices[rInd[r]][left]);
				r++;
			} while (r < MAXREF && lower_bound < bsf);


			if (r >= MAXREF && lower_bound < bsf)
			{

				abandon = 0;
				count = count + 1;
				d = distance(mkData[left], mkData[right], LENGTH, bsf);
				if (d < bsf)
				{
					t2 = clock();
					bsf = d;
					loc1 = left;
					loc2 = right;
					if (verbos)
						printf("New best-so-far is %lf and (%lld , %lld) are the new motif pair\n", bsf, loc1, loc2);

				}
			}
		}
	}

	if (verbos) {

		printf("\n\nFinal Motif is the pair ( %lld", loc1);
		printf(", %lld ) and the Motif distance is %lf\n", loc2, bsf);
		t2 = clock();
		printf("\nExecution Time was : %lf seconds\n", (t2 - t1) / CLOCKS_PER_SEC);
	}

	stdRef.clear();

	return bsf;
}
