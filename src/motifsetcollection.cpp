///\file motifsetcollection.cpp
///
///\brief File contains the motif set collection definitions.
///
///This is the source file of the motif set collection. The motif set collection consists of motif set class definitions.

#include <motifsetcollection.hpp>

BoxMotifSet::BoxMotifSet(const double &warp_in, const double &scale_in, const size_t &length_in, const double &height_in) : BasicMotifSet() {

	warp = warp_in;
	scale = scale_in;
	length = length_in;
	height = height_in;

	size_t realLength = warp * length;

	addNextValue(0.0);

	if (realLength > 2) {

		for (size_t itr = 0; itr < realLength - 2; itr++)
			addNextValue(scale * height);
	}

	addNextValue(0.0);
}

TriangleMotifSet::TriangleMotifSet(const double &warp_in, const double &scale_in, const size_t &length_in, const double &height_in) : BasicMotifSet() {

	warp = warp_in;
	scale = scale_in;
	length = length_in;
	height = height_in;

	size_t realLength = warp * length;

	addNextValue(0.0);

	if (realLength > 2) {

		double increase = scale * height / ((realLength - 1.0) / 2.0);

		for (size_t itr = 2; itr < realLength - 1; itr += 2)
			addNextValue(increase * (itr / 2.0));

		for (size_t itr = realLength - (realLength % 2 == 0 ? 2 : 1); itr > 1 && itr < (size_t)-1; itr -= 2)
			addNextValue(increase * (itr / 2.0));
	}

	addNextValue(0.0);
}

SemicircleMotifSet::SemicircleMotifSet(const double &warp_in, const double &scale_in, const size_t &length_in, const double &height_in) : BasicMotifSet() {

	warp = warp_in;
	scale = scale_in;
	length = length_in;
	height = height_in;

	if (height < 0.0) {

		height = -height;
		scale = -scale;
	}

	size_t realLength = warp * length;

	addNextValue(0.0);

	if (realLength > 2) {

		size_t diameter = realLength - 2;

		double scaleFactor = 2.0 * scale * height / diameter;
		size_t itr = diameter - 1;

		for (; itr > 0 && itr < (size_t)-1; itr -= 2)
			addNextValue(scaleFactor * sqrt((diameter / 2.0) * (diameter / 2.0) - (itr / 2.0) * (itr / 2.0)));

		if (itr == (size_t)-1)
			itr += 2;

		for (; itr <= diameter; itr += 2)
			addNextValue(scaleFactor * sqrt((diameter / 2.0) * (diameter / 2.0) - (itr / 2.0) * (itr / 2.0)));
	}

	addNextValue(0.0);
}

TrapezoidMotifSet::TrapezoidMotifSet(const double &warp_in, const double &scale_in, const size_t &length_in, const double &height_in) : BasicMotifSet() {

	warp = warp_in;
	scale = scale_in;
	length = length_in;
	height = height_in;

	size_t realLength = warp * length;

	addNextValue(0.0);

	if (realLength > 2) {
		size_t quarter = (realLength - 2) / 4;
		double increase = scale * height / (quarter + 1);

		for (size_t itr = 1; itr <= quarter; itr++)
			addNextValue(increase * itr);

		for (size_t itr = 0; itr < (realLength - 2) - 2 * quarter; itr++)
			addNextValue(scale * height);

		for (size_t itr = quarter; itr > 0; itr--)
			addNextValue(increase * itr);
	}

	addNextValue(0.0);
}

PositiveFlankMotifSet::PositiveFlankMotifSet(const double &warp_in, const double &scale_in, const size_t &length_in, const double &height_in) : BasicMotifSet() {

	warp = warp_in;
	scale = scale_in;
	length = length_in;
	height = height_in;

	size_t realLength = warp * length;

	addNextValue(0.0);

	if (realLength > 2) {

		double increase = scale * height / (realLength - 2.0);

		for (size_t itr = 1; itr < realLength - 1; itr++)
			addNextValue(increase * itr);
	}

	addNextValue(0.0);
}

NegativeFlankMotifSet::NegativeFlankMotifSet(const double &warp_in, const double &scale_in, const size_t &length_in, const double &height_in) : BasicMotifSet() {

	warp = warp_in;
	scale = scale_in;
	length = length_in;
	height = height_in;

	size_t realLength = warp * length;

	addNextValue(0.0);

	if (realLength > 2) {

		double increase = scale * height / (realLength - 2.0);

		for (size_t itr = realLength - 2; itr > 0; itr--)
			addNextValue(increase * itr);
	}

	addNextValue(0.0);
}

SineMotifSet::SineMotifSet(const double &warp_in, const double &scale_in, const size_t &length_in, const double &height_in) : BasicMotifSet() {

	warp = warp_in;
	scale = scale_in;
	length = length_in;
	height = height_in / 2.0;

	size_t realLength = warp * length;
	size_t middle = (realLength - 1) / 2;

	if (realLength > 2) {

		for (size_t itr = 0; itr < realLength - 1; itr++)
			if (realLength % 2 == 1 && itr == middle)
				addNextValue(0.0);
			else
				addNextValue(scale * height * sin((2 * M_PI) / (realLength - 1.0) * itr));
	}

	addNextValue(0.0);
}

CosineMotifSet::CosineMotifSet(const double &warp_in, const double &scale_in, const size_t &length_in, const double &height_in) : BasicMotifSet() {

	warp = warp_in;
	scale = scale_in;
	length = length_in;
	height = height_in / 2.0;

	size_t realLength = warp * length;

	if (realLength > 2)
		for (size_t itr = 0; itr < realLength; itr++)
			addNextValue((scale * height * sin(M_PI / 2.0 + (2 * M_PI) / (realLength - 1.0) * itr)) - scale * height);
}
