///\file outputgenerator.cpp
///
///\brief File contains the output generator class definition.
///
///This is the source file of the output generator. The output generator writes the time series and the motif set positions into output files.

#include <outputgenerator.hpp>

OutputGenerator::OutputGenerator() : OutputGenerator("time_series") {}

OutputGenerator::OutputGenerator(const string &outputFileName_in) 
#ifndef TEST_MODE
                                                                  : outputFileName("/" + outputFileName_in) {
	
	string outputDirName = "./time_series_0";
	int outputFolderNumber = 0;

#ifdef _WIN32

	DWORD ftyp = GetFileAttributesA(outputDirName.c_str());

	while (ftyp == FILE_ATTRIBUTE_DIRECTORY) {
		
		outputDirName = "./time_series_" + to_string(outputFolderNumber);
		ftyp = GetFileAttributesA(outputDirName.c_str());
		outputFolderNumber++;
	}

	CreateDirectory(outputDirName.c_str(), NULL);

#else

	while (!access(outputDirName.c_str(), 0)) {

		outputDirName = "./time_series_" + to_string(outputFolderNumber);
		outputFolderNumber++;
	}

	mkdir(outputDirName.c_str(), ACCESSPERMS);

#endif

	basicOutputFileName = outputFileName;
	outputFileMotifSetsName = outputDirName + outputFileName + "_motif_set_positions";
	outputFileGNUPlotScriptName = outputDirName + outputFileName + "_plot";
	outputFileName = outputDirName + outputFileName;

	basicOutputFileName.erase(0, 1);

#else
{
#endif
}

OutputGenerator::~OutputGenerator() {

	if (outputFile.is_open())
		outputFile.close();

	if (outputFileMotifSets.is_open())
		outputFileMotifSets.close();

	if (outputFileGNUPlotScript.is_open())
		outputFileGNUPlotScript.close();
}

void OutputGenerator::openNextFile() {

#ifndef TEST_MODE

	if (outputFile.is_open())
		outputFile.close();

	if (outputFileMotifSets.is_open())
		outputFileMotifSets.close();

	if (outputFileGNUPlotScript.is_open())
		outputFileGNUPlotScript.close();

	string tmpTimeSeriesFileName = outputFileName + "_" + to_string(outputFileNumber) + ".csv";
	string tmpFileMotifSetPositionsName = outputFileMotifSetsName + "_" + to_string(outputFileNumber) + ".csv";
	string tmpGNUPlotScriptName = outputFileGNUPlotScriptName + "_" + to_string(outputFileNumber) + ".plt";

	while (!access(tmpTimeSeriesFileName.c_str(), 0) || !access(tmpFileMotifSetPositionsName.c_str(), 0) || !access(tmpGNUPlotScriptName.c_str(), 0)) {
		
		outputFileNumber++;
		tmpTimeSeriesFileName = outputFileName + "_" + to_string(outputFileNumber) + ".csv";
		tmpFileMotifSetPositionsName = outputFileMotifSetsName + "_" + to_string(outputFileNumber) + ".csv";
		tmpGNUPlotScriptName = outputFileMotifSetsName + "_" + to_string(outputFileNumber) + ".plt";
	}

	outputFile.open(tmpTimeSeriesFileName, ios::out);
	outputFileMotifSets.open(tmpFileMotifSetPositionsName, ios::out);
	outputFileGNUPlotScript.open(tmpGNUPlotScriptName, ios::out);
	outputFileNumber++;

#endif
}

void OutputGenerator::printTimeSeriesHorizontal(const vector<double> &timeSeries_in, const vector<double> &d_in,
												const vector<size_t> &windowSizes_in, const vector<vector<size_t>> &motifSetPositions_in) {

#ifndef TEST_MODE

	if (timeSeries_in.size() == 0) {

		cerr << "ERROR: Time series is empty." << endl;
		exit(EXIT_FAILURE);
	}

	openNextFile();

	size_t itr = 1;
	double valueMin = DBL_MAX, valueMax = DBL_MIN;
	double tmpOutputValue;
	string tmpOutputString;

	tmpOutputValue = timeSeries_in.at(0);
	tmpOutputString = to_string(tmpOutputValue);
	outputFile.write(tmpOutputString.c_str(), tmpOutputString.size());

	while (itr < timeSeries_in.size()) {

		tmpOutputValue = timeSeries_in.at(itr);
		tmpOutputString = ", " + to_string(tmpOutputValue);
		outputFile.write(tmpOutputString.c_str(), tmpOutputString.size());

		if (tmpOutputValue > valueMax)
			valueMax = tmpOutputValue;
		if (tmpOutputValue < valueMin)
			valueMin = tmpOutputValue;

		itr++;
	}

	outputFile.close();


	tmpOutputString = "\n";

	Gnuplot g1("lines");

	g1.reset_plot();

	if (1.2 * (valueMax - valueMin) > 150.0)
		g1.savetosvg(2.0 * itr, 1.2 * (valueMax - valueMin), outputFileName + "_image_" + to_string(outputFileNumber - 1));
	else
		g1.savetosvg(2.0 * itr, 180.0, outputFileName + "_image_" + to_string(outputFileNumber - 1));
	g1.set_xrange(0, itr - 1);
	g1.set_yrange(valueMin - 0.2 * (valueMax - valueMin), valueMax + 0.2 * (valueMax - valueMin));
	g1.set_grid();

	string gnuplotCMD;
#ifdef _WIN32
	gnuplotCMD = "set terminal wxt size 1600, 300\n";
#elif __APPLE__
	gnuplotCMD = "set terminal aqua size 1600, 300\n";
#else
	gnuplotCMD = "set terminal qt size 1600, 300 persist\n";
#endif
	outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
	gnuplotCMD = "set xrange[" + to_string(0) + ":" + to_string(itr - 1) + "]\n";
	outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
	gnuplotCMD = "set yrange[" + to_string(valueMin - 0.2 * (valueMax - valueMin)) + ":" + to_string(valueMax + 0.2 * (valueMax - valueMin)) + "]\n";
	outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
	gnuplotCMD = "set grid\n";
	outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());


	if (d_in.size() == 0) {

		cerr << "ERROR: Distance vector is empty." << endl;
		exit(EXIT_FAILURE);
	}

	if (d_in.size() != motifSetPositions_in.size()) {

		cerr << "ERROR: Distance vector and motif set positions vector have different size." << endl;
		exit(EXIT_FAILURE);
	}

	if (windowSizes_in.size() == 0) {

		cerr << "ERROR: Window size vector is empty." << endl;
		exit(EXIT_FAILURE);
	}

	if (windowSizes_in.size() != d_in.size()) {

		cerr << "ERROR: Window size vector and distance vector have different size." << endl;
		exit(EXIT_FAILURE);
	}

	size_t maxMSSize = 0;


	for (size_t motifSetItr = 0; motifSetItr < d_in.size(); motifSetItr++) {

		if ((motifSetPositions_in.at(motifSetItr)).size() == 0) {

			cerr << "ERROR: Motif set positions vector is empty." << endl;
			exit(EXIT_FAILURE);
		}
		else if ((motifSetPositions_in.at(motifSetItr)).size() > maxMSSize)
			maxMSSize = (motifSetPositions_in.at(motifSetItr)).size();
	}

	for (size_t vectorItr = 0; vectorItr < d_in.size(); vectorItr++) {
        
        ostringstream streamObj;
        streamObj << d_in.at(vectorItr);
        
		tmpOutputString = streamObj.str();
		outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());

		tmpOutputString =  "," + to_string(windowSizes_in.at(vectorItr));
		outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());

		for (size_t positionItr = 0; positionItr < (motifSetPositions_in.at(vectorItr)).size(); positionItr++) {

			tmpOutputString = ", " + to_string((motifSetPositions_in.at(vectorItr)).at(positionItr));
			outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());

			g1.set_backgroundColorFromXOneToXTwo(motifSetPositions_in.at(vectorItr).at(positionItr),
				motifSetPositions_in.at(vectorItr).at(positionItr) + windowSizes_in.at(vectorItr) - 1);
			gnuplotCMD = "set obj rect from " + to_string(motifSetPositions_in.at(vectorItr).at(positionItr)) +
				", graph 0 to " + to_string(motifSetPositions_in.at(vectorItr).at(positionItr) + windowSizes_in.at(vectorItr) - 1) + ", graph 1 back fc lt -1 fs trans solid 0.1 noborder\n";
			outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
		}

		tmpOutputString = "\n";
		outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());
	}


	g1.set_style("lines").plothorizontalfile_x(outputFileName + "_" + to_string(outputFileNumber - 1) + ".csv",
		timeSeriesName + " " + to_string(outputFileNumber - 1));
	
	gnuplotCMD = "plot \"" + basicOutputFileName + "_" + to_string(outputFileNumber - 1) + ".csv\" matrix notitle with lines";
	outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
	outputFileGNUPlotScript.close();

	outputFileMotifSets.close();

#endif
}

void OutputGenerator::printTimeSeriesVertical(const vector<double> &timeSeries_in, const vector<double> &d_in,
											  const vector<size_t> &windowSizes_in, const vector<vector<size_t>> &motifSetPositions_in) {

#ifndef TEST_MODE

	if (timeSeries_in.size() == 0) {

		cerr << "ERROR: Time series is empty." << endl;
		exit(EXIT_FAILURE);
	}

	openNextFile();

	size_t itr = 0;
	double valueMin = DBL_MAX, valueMax = DBL_MIN;
	double tmpOutputValue;
	string tmpOutputString;

	while (itr < timeSeries_in.size()) {

		tmpOutputValue = timeSeries_in.at(itr);
		tmpOutputString = to_string(tmpOutputValue) + "\n";
		outputFile.write(tmpOutputString.c_str(), tmpOutputString.size());

		if (tmpOutputValue > valueMax)
			valueMax = tmpOutputValue;
		if (tmpOutputValue < valueMin)
			valueMin = tmpOutputValue;

		itr++;
	}

	outputFile.close();


	Gnuplot g1("lines");

	g1.reset_plot();

	if (1.2 * (valueMax - valueMin) > 150.0)
		g1.savetosvg(2.0 * itr, 1.2 * (valueMax - valueMin), outputFileName + "_image_" + to_string(outputFileNumber - 1));
	else
		g1.savetosvg(2.0 * itr, 180.0, outputFileName + "_image_" + to_string(outputFileNumber - 1));
	g1.set_xrange(0.0, itr - 1);
	g1.set_yrange(valueMin - 0.2 * (valueMax - valueMin), valueMax + 0.2 * (valueMax - valueMin));
	g1.set_grid();

	string gnuplotCMD;
#ifdef _WIN32
	gnuplotCMD = "set terminal wxt size 1600, 300\n";
#elif __APPLE__
	gnuplotCMD = "set terminal aqua size 1600, 300\n";
#else
	gnuplotCMD = "set terminal qt size 1600, 300 persist\n";
#endif
	outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
	gnuplotCMD = "set xrange[" + to_string(0) + ":" + to_string(itr - 1) + "]\n";
	outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
	gnuplotCMD = "set yrange[" + to_string(valueMin - 0.2 * (valueMax - valueMin)) + ":" + to_string(valueMax + 0.2 * (valueMax - valueMin)) + "]\n";
	outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
	gnuplotCMD = "set grid\n";
	outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());

	if (d_in.size() == 0) {

		cerr << "ERROR: Distance vector is empty." << endl;
		exit(EXIT_FAILURE);
	}

	if (d_in.size() != motifSetPositions_in.size()) {

		cerr << "ERROR: Distance vector and motif set positions vector have different size." << endl;
		exit(EXIT_FAILURE);
	}

	if (windowSizes_in.size() == 0) {

		cerr << "ERROR: Window size vector is empty." << endl;
		exit(EXIT_FAILURE);
	}

	if (windowSizes_in.size() != d_in.size()) {

		cerr << "ERROR: Window size vector and distance vector have different size." << endl;
		exit(EXIT_FAILURE);
	}

	size_t maxMSSize = 0;

	for (size_t motifSetItr = 0; motifSetItr < d_in.size(); motifSetItr++) {

		if ((motifSetPositions_in.at(motifSetItr)).size() == 0) {

			cerr << "ERROR: Motif set positions vector is empty." << endl;
			exit(EXIT_FAILURE);
		}
		else if ((motifSetPositions_in.at(motifSetItr)).size() > maxMSSize)
			maxMSSize = (motifSetPositions_in.at(motifSetItr)).size();
	}

    ostringstream streamObj;
    streamObj << d_in.at(0);

	tmpOutputString = streamObj.str();
	outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());

	for (size_t motifSetItr = 1; motifSetItr < d_in.size(); motifSetItr++) {
        
        ostringstream streamObj2;
        streamObj2 << d_in.at(motifSetItr);

		tmpOutputString = ", " + streamObj2.str();
		outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());
	}

	tmpOutputString = "\n";
	outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());


	tmpOutputString = to_string(windowSizes_in.at(0));
	outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());

	for (size_t motifSetItr = 1; motifSetItr < windowSizes_in.size(); motifSetItr++) {

		tmpOutputString = ", " + to_string(windowSizes_in.at(motifSetItr));
		outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());
	}

	tmpOutputString = "\n";
	outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());


	for (size_t positionItr = 0; positionItr < maxMSSize; positionItr++) {

		if (positionItr < motifSetPositions_in.at(0).size()) {

			tmpOutputString = to_string(motifSetPositions_in.at(0).at(positionItr));
			outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());
			
			g1.set_backgroundColorFromXOneToXTwo(motifSetPositions_in.at(0).at(positionItr),
				motifSetPositions_in.at(0).at(positionItr) + windowSizes_in.at(0) - 1);
			gnuplotCMD = "set obj rect from " + to_string(motifSetPositions_in.at(0).at(positionItr)) +
				", graph 0 to " + to_string(motifSetPositions_in.at(0).at(positionItr) + windowSizes_in.at(0) - 1) + ", graph 1 back fc lt -1 fs trans solid 0.1 noborder\n";
			outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
		}
		else {

			tmpOutputString = " ";
			outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());
		}

		for (size_t motifSetItr = 1; motifSetItr < windowSizes_in.size(); motifSetItr++) {

			if (positionItr < motifSetPositions_in.at(motifSetItr).size()) {

				tmpOutputString = ", " + to_string(motifSetPositions_in.at(motifSetItr).at(positionItr));
				outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());

				g1.set_backgroundColorFromXOneToXTwo(motifSetPositions_in.at(motifSetItr).at(positionItr),
					motifSetPositions_in.at(motifSetItr).at(positionItr) + windowSizes_in.at(motifSetItr) - 1);
				gnuplotCMD = "set obj rect from " + to_string(motifSetPositions_in.at(motifSetItr).at(positionItr)) +
					", graph 0 to " + to_string(motifSetPositions_in.at(motifSetItr).at(positionItr) + windowSizes_in.at(motifSetItr) - 1) + ", graph 1 back fc lt -1 fs trans solid 0.1 noborder\n";
				outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
			}
			else {

				tmpOutputString = ", ";
				outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());
			}
		}

		tmpOutputString = "\n";
		outputFileMotifSets.write(tmpOutputString.c_str(), tmpOutputString.size());
	}


	g1.set_style("lines").plotfile_x(outputFileName + "_" + to_string(outputFileNumber - 1) + ".csv", 1,
		timeSeriesName + " " + to_string(outputFileNumber - 1));

	gnuplotCMD = "plot \"" + basicOutputFileName + "_" + to_string(outputFileNumber - 1) + ".csv\" notitle with lines";
	outputFileGNUPlotScript.write(gnuplotCMD.c_str(), gnuplotCMD.size());
	outputFileGNUPlotScript.close();

	outputFileMotifSets.close();

#endif
}

void OutputGenerator::setTimeSeriesName(const string &timeSeriesName_in) {
	
	timeSeriesName = timeSeriesName_in;
}
