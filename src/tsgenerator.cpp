///\file tsgenerator.cpp
///
///\brief File contains the TSGenerator class definition.
///
///This is the source file of the TSGenerator. The TSGenerator consists of setter functions and the run() function. After configuring the TSGenerator with
///the setter functions one starts the time series generation by calling the run() function.


#include <tsgenerator.hpp>


TSGenerator::TSGenerator() {
    
	if (randomDevice.entropy())
		trueRandomEngineAvailable = true;
	else
		trueRandomEngineAvailable = false;
}

TSGenerator::~TSGenerator() {}

void TSGenerator::checkParsedArguments() {

	//check if the window size is to long for size_t
	if (windowSize + windowSize < windowSize) {

		cerr << "ERROR: Window size is to large." << endl;
		exit(EXIT_FAILURE);
	}

	//check if the time series length can handle the window size for at least two non-overlapping subsequences
	if (length < 3 * windowSize) {

		cerr << "ERROR: Window size is to large to fit into the time series." << endl;
		exit(EXIT_FAILURE);
	}

	//check if the time series length can handle all motif subsequences
	if (!motifSetVector.empty()) {

		size_t motifSetSubsequencesNumber = 0;
		size_t motifSetSubsequencesLength = 0;

		for (vector<motifSet>::iterator itr = motifSetVector.begin(); itr < motifSetVector.end(); itr++) {

			//check if all motif sets have at least 2 subsequences
			if ((*itr).amount < 2) {

				cerr << "ERROR: A single subsequence is not a motif set." << endl;
				exit(EXIT_SUCCESS);
			}

			//check if the sum of subsequences length is too large
			if (motifSetSubsequencesLength + windowSize < motifSetSubsequencesLength) {

				cerr << "ERROR: The sum of the subsequences length is too large." << endl;
				exit(EXIT_FAILURE);
			}

			motifSetSubsequencesLength += windowSize;
			motifSetSubsequencesNumber += (*itr).amount;
		}

		if (length < motifSetSubsequencesNumber * windowSize) {

			cerr << "ERROR: The sum of the subsequences length is to large to fit into the time series." << endl;
			exit(EXIT_FAILURE);
		}
	}
}

double TSGenerator::similarity(const vector<double> &timeSeriesOne_in, const vector<double> &timeSeriesTwo_in, double bestSoFar_in) {

	if (timeSeriesOne_in.size() != timeSeriesTwo_in.size()) {

		cerr << "ERROR: Time series has to be of same length to calculate similarity." << endl;
		exit(EXIT_FAILURE);
	}


	//calculate means
	double meanOne = 0.0;
	double meanTwo = 0.0;

	for (size_t itr = 0; itr < timeSeriesOne_in.size(); itr++) {

		meanOne += timeSeriesOne_in.at(itr);
		meanTwo += timeSeriesTwo_in.at(itr);
	}

	meanOne /= timeSeriesOne_in.size();
	meanTwo /= timeSeriesTwo_in.size();

	//calculate standard deviations
	double stdDevOne = 0.0;
	double stdDevTwo = 0.0;

	for (size_t itr = 0; itr < timeSeriesOne_in.size(); itr++) {

		stdDevOne += pow(timeSeriesOne_in.at(itr), 2);
		stdDevTwo += pow(timeSeriesTwo_in.at(itr), 2);
	}

	stdDevOne /= timeSeriesOne_in.size();
	stdDevTwo /= timeSeriesTwo_in.size();

	stdDevOne -= pow(meanOne, 2);
	stdDevTwo -= pow(meanTwo, 2);

	stdDevOne = sqrt(stdDevOne);
	stdDevTwo = sqrt(stdDevTwo);


	//z-normalize subsequences
	vector<double> normalizedTimeSeriesOne, normalizedTimeSeriesTwo;

	for (size_t itr = 0; itr < timeSeriesOne_in.size(); itr++) {

		normalizedTimeSeriesOne.push_back((timeSeriesOne_in.at(itr) - meanOne) / stdDevOne);
		normalizedTimeSeriesTwo.push_back((timeSeriesTwo_in.at(itr) - meanTwo) / stdDevTwo);
	}


	//calculate similarity
	double sumOfSquares = 0.0;
	double bestSoFar = pow(bestSoFar_in, 2);

	for (size_t i = 0; i < normalizedTimeSeriesOne.size() && sumOfSquares < bestSoFar; i++)
		sumOfSquares += pow(normalizedTimeSeriesOne.at(i) - normalizedTimeSeriesTwo.at(i), 2);

	return sqrt(sumOfSquares);
}

double TSGenerator::calculateNoise(normal_distribution<double> &randomValue_in, mt19937 &pseudoRandomEngine_in) {

	double tmpValue;

	if (noiseRatio > 0.0) {

		tmpValue = randomValue_in(pseudoRandomEngine_in);

		if (tmpValue < xOne)
			return xOne;
		else if (tmpValue > xTwo)
			return xTwo;
	}
	else
		return 0.0;

	return tmpValue;
}

void TSGenerator::calculateRawSubsequence(vector<double> &subsequence_out, size_t type_in, size_t windowSize_in, double height_in, double noiseRatio_in) {

	if (!subsequence_out.empty()) {

		subsequence_out.clear();
		subsequence_out.resize(0);
	}

	BasicMotifSet *tmpMotifSet;

	switch (type_in) {

	case 0:
		tmpMotifSet = new BoxMotifSet(1.0, 1.0, windowSize_in, height_in);
		break;
	case 1:
		tmpMotifSet = new TriangleMotifSet(1.0, 1.0, windowSize_in, height_in);
		break;
	case 2:
		tmpMotifSet = new SemicircleMotifSet(1.0, 1.0, windowSize_in, height_in);
		break;
	case 3:
		tmpMotifSet = new TrapezoidMotifSet(1.0, 1.0, windowSize_in, height_in);
		break;
	case 4:
		tmpMotifSet = new PositiveFlankMotifSet(1.0, 1.0, windowSize_in, height_in);
		break;
	case 5:
		tmpMotifSet = new NegativeFlankMotifSet(1.0, 1.0, windowSize_in, height_in);
		break;
	case 6:
		tmpMotifSet = new SineMotifSet(1.0, 1.0, windowSize_in, height_in);
		break;
	case 7:
		tmpMotifSet = new CosineMotifSet(1.0, 1.0, windowSize_in, height_in);
		break;
	}

	tmpMotifSet->getSequence(subsequence_out);

	if (noiseRatio_in > numeric_limits<double>::min()) {

		unsigned int randomSeed = chrono::system_clock::now().time_since_epoch().count();

		if (trueRandomEngineAvailable)
			randomSeed = randomDevice();

		normal_distribution<double> randomNoise(0.0, noiseRatio_in < numeric_limits<double>::min() ? numeric_limits<double>::min() :
			noiseRatio_in * (xTwo - xOne) / 2.0);
		mt19937 pseudoRandomEngine(randomSeed);

		for (size_t itr = 0; itr < subsequence_out.size(); itr++)
			subsequence_out.at(itr) = subsequence_out.at(itr) + randomNoise(pseudoRandomEngine);
	}
}

double TSGenerator::calculateRange(const vector<double> &subsequence_in, const vector<vector<double>> &subsequences_in) {

	double largestSoFar, d;

	largestSoFar = similarity(subsequence_in, subsequences_in.at(0), numeric_limits<double>::max());

	for (size_t itr = 0; itr < subsequences_in.size(); itr++)
		if (largestSoFar < (d = similarity(subsequence_in, subsequences_in.at(itr), numeric_limits<double>::max())))
			largestSoFar = d;
	
	return largestSoFar;
}

size_t TSGenerator::calculateRandomPosition(vector<freeTimeSeriesPosition> &freePositions_in, size_t windowSize_in) {

	size_t freeTimeSeriesLength = 0;

	for (size_t itr = 0; itr < freePositions.size(); itr++)
		if (freePositions.at(itr).length >= windowSize_in)
			freeTimeSeriesLength += freePositions.at(itr).length - windowSize_in + 1;

	if (!freeTimeSeriesLength) {

		cerr << "ERROR: There are no free positions in the time series fitting all motif set subsequences." << endl;
		exit(EXIT_FAILURE);
	}

	uniform_int_distribution<size_t> randomPosition(1, freeTimeSeriesLength);
	unsigned int randomSeed = chrono::system_clock::now().time_since_epoch().count();

	if (trueRandomEngineAvailable)
		randomSeed = randomDevice();

	mt19937 pseudoRandomPositionEngine(randomSeed);

	size_t randomCalculatedPosition = randomPosition(pseudoRandomPositionEngine);
	size_t position = 0;
	size_t freeSubsequenceNumber;

	for (size_t itr = 0; itr < freePositions.size() && randomCalculatedPosition > 0; itr++) {

		if (freePositions.at(itr).length >= windowSize_in) {

			if (randomCalculatedPosition > freePositions.at(itr).length - windowSize_in + 1)
				randomCalculatedPosition -= freePositions.at(itr).length - windowSize_in + 1;
			else {

				position = freePositions.at(itr).position + randomCalculatedPosition - 1;
				randomCalculatedPosition = 0;
			}
		}

		freeSubsequenceNumber = itr;
	}

	size_t oldPosition = freePositions.at(freeSubsequenceNumber).position;
	size_t oldLength = freePositions.at(freeSubsequenceNumber).length;

	freePositions.erase(freePositions.begin() + freeSubsequenceNumber);

	if (position + windowSize_in < oldPosition + oldLength) {

		freeTimeSeriesPosition firstNewFreeSubsequence;
		firstNewFreeSubsequence.position = position + windowSize_in;
		firstNewFreeSubsequence.length = oldLength - position + oldPosition - windowSize_in;
		freePositions.insert(freePositions.begin() + freeSubsequenceNumber, firstNewFreeSubsequence);
	}

	if (oldPosition < position) {

		freeTimeSeriesPosition secondNewFreeSubsequence;
		secondNewFreeSubsequence.position = oldPosition;
		secondNewFreeSubsequence.length = position - oldPosition;
		freePositions.insert(freePositions.begin() + freeSubsequenceNumber, secondNewFreeSubsequence);
	}
	
	return position;
}

void TSGenerator::addNoiseToSubsequence(vector<double> &subsequence, double noiseRatio_in) {

	if (noiseRatio_in >= numeric_limits<double>::min()) {

		unsigned int randomSeed = chrono::system_clock::now().time_since_epoch().count();

		if (trueRandomEngineAvailable)
			randomSeed = randomDevice();

		normal_distribution<double> randomNoise(0.0, noiseRatio_in < numeric_limits<double>::min() ? numeric_limits<double>::min() :
			noiseRatio_in * (xTwo - xOne) / 2.0);
		mt19937 pseudoRandomEngine(randomSeed);

		for (size_t itr = 0; itr < subsequence.size(); itr++)
			subsequence.at(itr) = subsequence.at(itr) + randomNoise(pseudoRandomEngine);
	}
}

void TSGenerator::generateRandomTS(vector<double> &timeSeries_out) {

	if (!(timeSeries_out.empty())) {

		timeSeries_out.clear();
		timeSeries_out.resize(0);
	}

	unsigned int randomSeed = chrono::system_clock::now().time_since_epoch().count();

	if (trueRandomEngineAvailable)
		randomSeed = randomDevice();

	normal_distribution<double> randomValue(0.0, noiseRatio < numeric_limits<double>::min() ? numeric_limits<double>::min() : noiseRatio * (xTwo - xOne) / 2.0);
	mt19937 pseudoRandomEngine(randomSeed);

	randomSeed = chrono::system_clock::now().time_since_epoch().count();

	if (trueRandomEngineAvailable)
		randomSeed = randomDevice();

	normal_distribution<double> randomNoise(0.0, noiseRatio < numeric_limits<double>::min() ? numeric_limits<double>::min() : noiseRatio * (xTwo - xOne) / 2.0);
	mt19937 pseudoRandomNoiseEngine(randomSeed);

	//generate first random synthetic time series value
	double value = 0, tmpValue;

	for (int i = 0; i < 100; i++) {

		tmpValue = randomValue(pseudoRandomEngine);

		if (value + tmpValue > baseValue + (xTwo - xOne) || value + tmpValue < baseValue - (xTwo - xOne)) {

			if (value - tmpValue > baseValue + (xTwo - xOne) || value - tmpValue < baseValue - (xTwo - xOne))
				value = -value;
			else
				value -= tmpValue;
		}
		else
			value += tmpValue;
	}

	//do the random walk
	timeSeries_out.push_back(baseValue + value + calculateNoise(randomNoise, pseudoRandomNoiseEngine));

	for (size_t itr = 1; itr < length; itr++) {

		tmpValue = randomValue(pseudoRandomEngine);

		if (value + tmpValue > baseValue + (xTwo - xOne) || value + tmpValue < baseValue - (xTwo - xOne)) {

			if (value - tmpValue > baseValue + (xTwo - xOne) || value - tmpValue < baseValue - (xTwo - xOne))
				value = -value;
			else
				value -= tmpValue;
		}
		else
			value += tmpValue;

		timeSeries_out.push_back(baseValue + value + calculateNoise(randomNoise, pseudoRandomNoiseEngine));
	}
}

bool TSGenerator::searchForUnintentionalMatches(const vector<double> &timeSeries_in, const vector<size_t> &motifSetPositions_in, double similarity_in) {
	
	vector<size_t> notMotifSetPositions;
	bool flag = true;

	for (size_t itr = 0; itr < length - windowSize + 1; itr++) {

		for (size_t position : motifSetPositions_in) {

			if (windowSize < position) {

				if (position - windowSize < itr && itr < position + windowSize) {

					itr = position + windowSize - 1;
					flag = false;
				}
			}
			else {

				if (itr <= position) {

					itr = position + windowSize - 1;
					flag = false;
				}
			}
		}

		if (flag) {

			notMotifSetPositions.push_back(itr);
		}
		
		flag = true;
	}

	double sim;
	bool returnValue = false;

	for (size_t notMotifSetPosition : notMotifSetPositions) {

		vector<double> remSub;

		for (size_t tsItrRemSub = notMotifSetPosition; tsItrRemSub <= notMotifSetPosition + windowSize - 1; tsItrRemSub++)
			remSub.push_back(timeSeries_in.at(tsItrRemSub));

		for (size_t itr = (motifSetPositions_in.back() - windowSize + 1 < motifSetPositions_in.back() ? motifSetPositions_in.back() - windowSize + 1 : 0);
			itr < (motifSetPositions_in.back() + windowSize - 1 > motifSetPositions_in.back() ? motifSetPositions_in.back() + windowSize - 1 : -1 - windowSize);
			itr++) {

			if (itr != motifSetPositions_in.back()) {

				vector<double> newSub;

				for (size_t tsItrNewSub = itr; tsItrNewSub < itr + windowSize; tsItrNewSub++)
					newSub.push_back(timeSeries_in.at(tsItrNewSub));

				sim = similarity(newSub, remSub, similarity_in);

				if (sim < similarity_in) {

					returnValue = true;
					break;
				}
			}
		}

		if (returnValue)
			break;
	}

	return returnValue;
}

void TSGenerator::runMotifPair(vector<double> &timeSeries_out, vector<double> &d_out, vector<size_t> &windowSize_out,
							   vector<vector<size_t>> &motifSetPositions_out, const string type_in, double height_in) {

#ifndef TEST_MODE

	//empty motif set vector since we are generating a top motif pair
	if (!motifSetVector.empty()) {

		motifSetVector.clear();
		motifSetVector.resize(0);
	}

	//check parameters for consistency
	checkParsedArguments();


	//parse the type input
	uniform_int_distribution<size_t> randomType(0, definedMotifSetTypes.size() - 1);

	unsigned int randomSeed = chrono::system_clock::now().time_since_epoch().count();

	if (trueRandomEngineAvailable)
		randomSeed = randomDevice();

	mt19937 pseudoRandomTypeEngine(randomSeed);

	vector<string>::iterator tmpItr;
	int type = 0;

	if (!type_in.compare("random"))
		type = randomType(pseudoRandomTypeEngine);
	else if ((tmpItr = find(definedMotifSetTypes.begin(), definedMotifSetTypes.end(), type_in)) != definedMotifSetTypes.end())
		type = distance(definedMotifSetTypes.begin(), tmpItr);
	else {

		cerr << "ERROR: Wrong motif pair subsequence type: " << type_in << endl;
		exit(EXIT_FAILURE);
	}

	//generate random time series
	generateRandomTS(timeSeries_out);

	//declaration stuff
	freeTimeSeriesPosition freePosition;
	freePosition.position = 0;
	freePosition.length = timeSeries_out.size();
	freePositions.push_back(freePosition);

	motifSetPositions_out.resize(1);
	size_t positionOne = -1;
	size_t positionTwo = -1;


	//decleration suff
	vector<double> baseSubsequence;
	vector<double> firstSubsequence;
	double noise = noiseRatio;
	windowSize_out.push_back(windowSize);
	double difference;


	//calculate first random position in the synthetic time series
	size_t position = calculateRandomPosition(freePositions, windowSize);

	motifSetPositions_out.at(0).push_back(position);


	//update the synthetic time series
	difference = timeSeries_out.at(position) - timeSeries_out.at(position + windowSize - 1);

	for (size_t itr = position + windowSize; itr < timeSeries_out.size(); itr++)
		timeSeries_out.at(itr) = timeSeries_out.at(itr) + difference;


	//calculate first subsequence
	calculateRawSubsequence(baseSubsequence,
							type,
							windowSize,
							height_in,
							noise);

	firstSubsequence.clear();
	firstSubsequence.resize(baseSubsequence.size());

	for (size_t itr = position; itr < position + windowSize; itr++)
		firstSubsequence.at(itr - position) = timeSeries_out.at(position) + baseSubsequence.at(itr - position);


	//add subsequence to synthetic time series
	for (size_t itr = position; itr < position + windowSize; itr++)
		timeSeries_out.at(itr) = firstSubsequence.at(itr - position);

	//determine similarity of the top motif pair in the random synthetic time series
	double d = mk(timeSeries_out, windowSize, positionOne, positionTwo);


	//calculate second free position in the synthetic time series
	position = calculateRandomPosition(freePositions, windowSize);

	motifSetPositions_out.at(0).push_back(position);


	//update synthetic time series
	difference = timeSeries_out.at(position) - timeSeries_out.at(position + windowSize - 1);

	for (size_t itr = position + windowSize; itr < timeSeries_out.size(); itr++)
		timeSeries_out.at(itr) = timeSeries_out.at(itr) + difference;


	//calculate the second top motif pair subsequence
	noise = noiseRatio;
	double subsequenceFirstValue = timeSeries_out.at(position);

	for (size_t itr = 0; itr < 1002; itr++) {

		//copy the first subsequence
		vector<double> secondSubsequence(firstSubsequence);

		for (size_t itr = position; itr < position + windowSize; itr++)
			secondSubsequence.at(itr - position) = subsequenceFirstValue + secondSubsequence.at(itr - position);

		addNoiseToSubsequence(secondSubsequence, noise);


		//check if the similarity of first subsequence and second subsequence is smaller then d
		if ((similarity(firstSubsequence, secondSubsequence, d)) < d) {

			//add subsequence to synthetic time series
			for (size_t itr = position; itr < position + windowSize; itr++)
				timeSeries_out.at(itr) = secondSubsequence.at(itr - position);
			
			d = mk(timeSeries_out, windowSize, positionOne, positionTwo);

			if ((motifSetPositions_out.at(0).at(0) == positionOne && motifSetPositions_out.at(0).at(1) == positionTwo) ||
				(motifSetPositions_out.at(0).at(1) == positionOne && motifSetPositions_out.at(0).at(0) == positionTwo)) {

				d_out.push_back(d);
				break;
			}
		}
		else {

			if (noise / 1000.0 < numeric_limits<double>::min())
				noise = numeric_limits<double>::min();
			else
				noise -= noiseRatio / 1000.0;
		}

		if (itr == 1001) {

			cerr << "ERROR: Cannot add time series motif pair. Retry or change your settings!" << endl;
			exit(EXIT_FAILURE);
		}
	}

#endif
}

void TSGenerator::runMotifSets(vector<double> &timeSeries_out, vector<double> &d_out, vector<size_t> &windowSize_out, vector<vector<size_t>> &motifSetPositions_out) {
	
#ifndef TEST_MODE

	//check parameters for consistency
	checkParsedArguments();

	//generate random time series
	generateRandomTS(timeSeries_out);


	//declaration stuff
	freeTimeSeriesPosition freePosition;
	freePosition.position = 0;
	freePosition.length = timeSeries_out.size();
	freePositions.push_back(freePosition);

	motifSetPositions_out.resize(motifSetVector.size());
	size_t motifSetNumber = 0;
	size_t positionOne = -1;
	size_t positionTwo = -1;
	
	//determine similarity of the top motif pair in the random synthetic time series
	double d = mk(timeSeries_out, windowSize, positionOne, positionTwo);

	//induce motif sets into synthetic time series
	while (motifSetNumber < motifSetVector.size()) {

		//declaration stuff
		double noise = noiseRatio;
		vector<double> motifSetCenter;
		vector<double> subsequence;

		bool repeatLoop = false;


		//calculate the center of the motif set in the window size dimentional room of subsequence values
		do {
			
			//declaration stuff
			repeatLoop = false;
			

			//calculate temporary motif set center subsequence in the window size dimentional room of subsequence values
			calculateRawSubsequence(motifSetCenter,
								 motifSetVector.at(motifSetNumber).type    ,
								 windowSize                                ,
								 motifSetVector.at(motifSetNumber).height  ,
					             noise                                     );

			noise += noiseRatio / 10;


			//check if there is no other subsequence in the range 3.0 * d / 2.0 around the center subsequence
			for (size_t itr = 0;
				itr < timeSeries_out.size() - windowSize;
				itr++) {
					
				vector<double> tmpSubsequence(timeSeries_out.begin() +
											  itr                       ,
											  timeSeries_out.begin() +
											  itr                    +
											  windowSize                );

				if (similarity(motifSetCenter                 ,
							   tmpSubsequence                 ,
							   numeric_limits<double>::max()  ) <=
					3.0 * d / 2.0                                        )
					repeatLoop = true;
			}

			//otherwise repeat
		} while(repeatLoop);

		subsequence.clear();
		subsequence.resize(motifSetCenter.size());


		//declaration stuff
		size_t position;
		double difference;


		//calculate the subsequences of the motif set
		windowSize_out.push_back(motifSetCenter.size());
		
		for (size_t itr = 0; itr < (motifSetVector.at(motifSetNumber)).amount; itr++) {

			//calculate the random position in the synthetic time series
			position = calculateRandomPosition(freePositions, windowSize);
			
			motifSetPositions_out.at(motifSetNumber).push_back(position);


			//calculate the difference between first and last value of the subsequence in the time series
			difference = timeSeries_out.at(position) - timeSeries_out.at(position + windowSize - 1);

			//update time series after subsequence
			for (size_t itr = position + windowSize; itr < timeSeries_out.size(); itr++)
				timeSeries_out.at(itr) = timeSeries_out.at(itr) + difference;

			//store the first value of the subsequence
			double subsequenceFirstValue = timeSeries_out.at(position);
			
			//check if all motif set subsequences are in range smaller than r / 2.0
			for (size_t itr = 0; itr < 55; itr++) {

				//copy the motif set center subsequence
				vector<double> newSubsequence(motifSetCenter);

				for (size_t itr = position; itr < position + windowSize; itr++)
					newSubsequence.at(itr - position) = subsequenceFirstValue + newSubsequence.at(itr - position);

				addNoiseToSubsequence(newSubsequence, noise);


				//check if the similarity of the subsequence and the center subsequence is at most d / 2.0
				if (similarity(newSubsequence, motifSetCenter, d / 2.0) < d / 2.0) {

					subsequence = newSubsequence;

					//add subsequence to synthetic time series
					for (size_t itr = position; itr < position + windowSize; itr++)
						timeSeries_out.at(itr) = subsequence.at(itr - position);
					
					if (!searchForUnintentionalMatches(timeSeries_out, motifSetPositions_out.at(motifSetNumber), d))
						break;
				}
				else {

					if (noise / 50.0 < numeric_limits<double>::min())
						noise = numeric_limits<double>::min();
					else
						noise -= noiseRatio / 50.0;
				}
				
				if (itr == 55) {

					cerr << "ERROR: Cannot add another motif set of size " << (motifSetVector.at(motifSetNumber)).amount <<
							". Retry or change your settings!" << endl;
					exit(EXIT_FAILURE);
				}
			}
		}


		d_out.push_back(d / 2.0);
		
		motifSetNumber++;
	}
	
	//add top motif pair to output
	motifSetPositions_out.resize(motifSetPositions_out.size() + 1);

	positionOne = 0;
	positionTwo = 0;

	d_out.push_back(mk(timeSeries_out, windowSize, positionOne, positionTwo));
	motifSetPositions_out.at(motifSetNumber).push_back(positionOne);
	motifSetPositions_out.at(motifSetNumber).push_back(positionTwo);
	windowSize_out.push_back(windowSize);
		
	motifSetNumber++;

#endif
}

void TSGenerator::setRandomness(const double &noiseRatio_in) {

	if (noiseRatio_in > 0.0)
		noiseRatio = noiseRatio_in;
	else
		noiseRatio = 0.0;
}

void TSGenerator::setRange(const double &xOne_in, const double &xTwo_in) {

	if (abs(xTwo_in - xOne_in) < 0.001) {

		cerr << "ERROR: Range is too small." << endl;
		exit(EXIT_FAILURE);
	}

	if (xOne_in < xTwo_in) {

		xOne = xOne_in;
		xTwo = xTwo_in;
	}
	else {
		
		xTwo = xOne_in;
		xOne = xTwo_in;
	}

	baseValue = (xOne + xTwo) / 2.0;
}

void TSGenerator::setLength(size_t length_in) {

	if (length_in < 100) {

		cerr << "ERROR: Time series length is to short." << endl;
		exit(EXIT_FAILURE);
	}

	length = length_in;
}

void TSGenerator::setWindowSize(size_t windowSize_in) {

	if (windowSize_in < 1) {

		cerr << "ERROR: Window size is to short." << endl;
		exit(EXIT_FAILURE);
	}

	windowSize = windowSize_in;
}

void TSGenerator::setMotifSetVector(const vector<string> &motifSetVector_in) {

	if (!motifSetVector.empty()) {

		motifSetVector.clear();
		motifSetVector.resize(0);
	}

	uniform_int_distribution<size_t> randomType(0, definedMotifSetTypes.size() - 1);

	unsigned int randomSeed = chrono::system_clock::now().time_since_epoch().count();

	if (trueRandomEngineAvailable)
		randomSeed = randomDevice();

	mt19937 pseudoRandomTypeEngine(randomSeed);

	vector<string>::const_iterator motifSetVectorItr = motifSetVector_in.begin();

	while (motifSetVectorItr != motifSetVector_in.end()) {
		
		motifSet newMotifSet;
		vector<string>::iterator tmpItr;

		if (!(*motifSetVectorItr).compare("random"))
			newMotifSet.type = randomType(pseudoRandomTypeEngine);
		else if ((tmpItr = find(definedMotifSetTypes.begin(), definedMotifSetTypes.end(), *motifSetVectorItr)) != definedMotifSetTypes.end())
			newMotifSet.type = distance(definedMotifSetTypes.begin(), tmpItr);
		else {

			cerr << "ERROR: Wrong motif set type: " << (*motifSetVectorItr) << endl;
			exit(EXIT_FAILURE);
		}

		
		motifSetVectorItr++;

		if (motifSetVectorItr == motifSetVector_in.end()) {

			cerr << "ERROR: Missing amount of motif set!" << endl;
			exit(EXIT_FAILURE);
		}

		if ((*motifSetVectorItr).find_first_not_of("0123456789") != string::npos) {

			cerr << "ERROR: " << (*motifSetVectorItr) << " is not a valid integer!" << endl;
			exit(EXIT_FAILURE);
		}
		else
			newMotifSet.amount = stoll(*motifSetVectorItr);


		motifSetVectorItr++;

		if (motifSetVectorItr == motifSetVector_in.end()) {

			cerr << "ERROR: Missing height of motif set!" << endl;
			exit(EXIT_FAILURE);
		}

		if (!check_if_float(*motifSetVectorItr)) {

			cerr << "ERROR: " << (*motifSetVectorItr) << " is not a valid float!" << endl;
			exit(EXIT_FAILURE);
		}
		else
			newMotifSet.height = stold(*motifSetVectorItr);


		motifSetVectorItr++;

		motifSetVector.push_back(newMotifSet);
	}

	//sort the motSetVector by motif set size, greatest motif set size left/first
	if (!motifSetVector.empty())
		sort(motifSetVector.begin()                           ,
			 motifSetVector.end()                             ,
			 [](const motifSet& lhs, const motifSet& rhs) {

				 return lhs.amount > rhs.amount;
			  }                                               );
}
